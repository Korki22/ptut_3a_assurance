<?php
	session_start();
?>

<?php
	// Gestion des variables
	$login = htmlspecialchars($_POST['login']);
	$pass = htmlspecialchars($_POST['pass']);


	//Connection bdd
	include ("connexion.php");

	$base = "ptut_db";
    $sqlUserSalarie = "SELECT * FROM Salarie WHERE Email_salarie = '".$login."' and Pass_salarie = '".sha1($pass)."'";
    $sqlUserEntreprise = "SELECT * FROM Entreprise WHERE Email_entreprise = '".$login."' and Pass_entreprise = '".sha1($pass)."'";

    // On selectionne tous les utilisateurs avec le login et le pass du formulaire
    // Cela renvoi le nombre de comptes portant le m�me nom et mot de passe que celui du formulaire
	$requeteSalarie = connectDB($base, $sqlUserSalarie);
	if (is_null($requeteSalarie)) {
		$cptSalarie = 0;
	}
	else {
		$cptSalarie = $requeteSalarie->num_rows;
	}
	$resultatSalarie = $requeteSalarie->fetch_array();

	$requeteEntreprise = connectDB($base, $sqlUserEntreprise);
	if (is_null($requeteEntreprise)) {
		$cptEntreprise = 0;
	}
	else {
		$cptEntreprise = $requeteEntreprise->num_rows;
	}
	$resultatEntreprise = $requeteEntreprise->fetch_array();

	// Si le compte existe
	if ($cptSalarie == 1 OR $cptEntreprise == 1) {
		if ($cptSalarie == 1) {
			$_SESSION['isConnected'] = 1;
			$name = $resultatSalarie['ID_Salarie'];
			$nom = $resultatSalarie['Prenom_salarie']; 
		}
		else {
			if ($cptEntreprise == 1) {
				$_SESSION['isConnected'] = 2;
				$name = $resultatEntreprise['ID_entreprise'];
				$nom = $resultatEntreprise['Raison_sociale']; 
			}
		}
		// On initialise les variables de session
		$_SESSION['login'] = $login;
		$_SESSION['id'] = $name;
		$_SESSION['nom'] = $nom;
		$_SESSION['error'] = 0;

		// On cr�e les cookies
		setcookie('login', $_SESSION['login'], time() +1200, null, null, false, true);
		setcookie('name', $_SESSION['name'], time() +1200, null, null, false, true);
		setcookie('nom', $_SESSION['nom'], time() +1200, null, null, false, true);

		$requeteSalarie->free();
		$requeteEntreprise->free();

		?>

		// On retourne � la page d'accueil
		<script language=javascript>
			window.close();
		</script>
	<?php
	}
	// Si il n'existe pas, on renvoie dans login.php que la connexion a echou�
	else {
		$_SESSION['isConnected'] = 0;
		$_SESSION['error'] = 1;

		header('location: test.php');
	}
?>