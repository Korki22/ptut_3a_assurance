<?php
session_start();
    require_once('base.php');
    $bdd = connect_db();
    if(isset($_SESSION['id'])){
        $entiteencours = $_SESSION['id'];
    } else  $entiteencours = 1;
    
    $id_salarie = $_SESSION["id"];
	$mail_salarie = $_POST["Email_salarie"];
	$mdp1 = $_POST["Pass_salarie"];
	$mdp2 = $_POST["Pass_salarie2"];
    $nom_salarie = $_POST["Nom_salarie"];
    $prenom_salarie = $_POST["Prenom_salarie"];
    $date_naissance_salarie = $_POST["Date_naissance_salarie"];
    
    $id_situation_familiale = $_POST["Id_situation_familiale"];
	$nombre_enfants_salarie = $_POST["Nombre_enfants_salarie"];
	$num_tel_salarie = $_POST["Num_tel_salarie"];
	$nom_rue_salarie = $_POST["Nom_rue_salarie"];
	$ville_salarie = $_POST["Ville_salarie"];
	$cp_salarie = $_POST["CP_salarie"];
	$nom_conjoint_salarie = $_POST["Nom_conjoint_salarie"];
	$prenom_conjoint_salarie = $_POST["Prenom_conjoint_salarie"];
	$date_naissance_conjoint_salarie = $_POST["Date_naissance_conjoint_salarie"];
	
	// Le mode de versement ne peut pas être modifié une fois qu'il est renseigné ("if" lors de la 1° modification de l'employe)
	if (isset($_POST["Mode_versement_salarie"])) {
		$modeVersement = $_POST["Mode_versement_salarie"];
		$modifModeVersement = ", ID_mode_versement=".$modeVersement;
	} else $modifModeVersement = "";
	
	$erreur = "";
	echo $num_tel_salarie;
	
	// Si mail renseigné dans le formulaire
	if ($mail_salarie != "")
	{
		$reqMailUtilise = $bdd->prepare("select * from Salarie where ID_salarie <> ".$id_salarie." and Email_salarie = '".$mail_salarie."'");
		$reqMailUtilise->execute() or die (print_r($reqMailUtilise->errorInfo()));
		$mail_utilise = $reqMailUtilise->fetch(PDO::FETCH_ASSOC);
		
		// Si mail utilisable (pas déjà enregistré dans la base)
		if ($mail_utilise["ID_salarie"] == null)
		{
			// Pas de mdp à changer :
			if (($mdp1 == "") && ($mdp2 == ""))
			{
				$reqModifSalarie = $bdd->prepare("update Salarie set Email_salarie='".$mail_salarie."', Nom_salarie='".$nom_salarie."', Prenom_salarie='".$prenom_salarie."', "
					."Date_naissance_salarie='".$date_naissance_salarie."', Id_situation_familiale='".$id_situation_familiale."', Nombre_enfants_salarie='".$nombre_enfants_salarie."', "
					."Num_tel_salarie=".$num_tel_salarie.", Nom_rue_salarie='".$nom_rue_salarie."', Ville_salarie='".$ville_salarie."', CP_salarie='".$cp_salarie."', Nom_conjoint_salarie='".$nom_conjoint_salarie."', "
					."Prenom_conjoint_salarie='".$prenom_conjoint_salarie."', Date_naissance_conjoint_salarie='".$date_naissance_conjoint_salarie."'".$modifModeVersement." where ID_salarie = ".$id_salarie.";");
					
			// Mdp à changer :
			} else if (($mdp1 != "") && ($mdp2 != "")){
				
				// Pas d'erreur entre le mot de passe et la confirmation
				if ($mdp1 == $mdp2)
				{
					$mdp = sha1($mdp1);
					$reqModifSalarie = $bdd->prepare("update Salarie set Email_salarie='".$mail_salarie."', Pass_salarie='".$mdp."', Nom_salarie='".$nom_salarie."', Prenom_salarie='".$prenom_salarie."', "
					."Date_naissance_salarie='".$date_naissance_salarie."', Id_situation_familiale='".$id_situation_familiale."', Nombre_enfants_salarie='".$nombre_enfants_salarie."', "
					."Num_tel_salarie=".$num_tel_salarie.", Nom_rue_salarie='".$nom_rue_salarie."', Ville_salarie='".$ville_salarie."', CP_salarie='".$cp_salarie."', Nom_conjoint_salarie='".$nom_conjoint_salarie."', "
					."Prenom_conjoint_salarie='".$prenom_conjoint_salarie."', Date_naissance_conjoint_salarie='".$date_naissance_conjoint_salarie."'".$modifModeVersement." where ID_salarie = ".$id_salarie.";");
					
				} else $erreur = "Veuillez indiquer correctement le mot de passe";
			} else $erreur = "Veuillez indiquer correctement le mot de passe";
		} else $erreur = "Ce mail est déja utilisé";
	} else $erreur = "Vous devez renseigner l'adresse mail.";
	
	// Si pas d'erreur, execution et message de succès sinon message d'erreur
	if ($erreur == "") {
		$reqModifSalarie->execute() or die (print_r($reqModifSalarie->errorInfo()));
		$_SESSION["message"] = "Les modifications ont été effectuées avec succès";
	} else $_SESSION["erreur"] = $erreur;
	
	
	
    
    header('Location: http://localhost/ptut_3a_assurance/php/ptut_3a_assurance/index.php?content=employe');