
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
	<link rel="stylesheet" href="Bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="Bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="Bootstrap/css/bootstrap-theme.css">
        <link rel="stylesheet" href="Bootstrap/css/bootstrap-theme.min.css">
        <script type="text/javascript"> 
            function Afficher(btn,div1,div2) 
            { 
             if (btn.checked) 
               { 
                document.getElementById(div1).style.display="inline"; 
             document.getElementById(div2).style.display="none"; 
               } 
             else { 
                document.getElementById(div1).style.display="none"; 
             document.getElementById(div2).style.display="display"; 
               } 
            } 
        </script> 
        <script type="text/javascript">
            var fieldalias="mot de passe"
            // On Déclare dans la variable fieldalias le type de texte entré

            function verify(element1, element2) {
              var passed=false
            // On va donner à la variable passed la valeur false (fausse).

               if (element1.value=='')
            // si le premier champ est vide (Propriété value vide) 
               {
                alert("Veuillez entrer votre "+fieldalias+" dans le premier champ!")
            // c'est pas bien il faut le remplir :-)
            // On ouvre donc une boite d'alerte grâce à la méthode alert de l'objet window
                element1.focus()
            // et on y place le curseur grâce à la méthode focus
               }

            // puis on va faire exactement la même chose pour le second champ
               else if (element2.value=='')
               {
                alert("Veuillez confirmer votre "+fieldalias+" dans le second champ!")
                element2.focus()
               }

               else if (element1.value!=element2.value)
            /* Si les valeurs des 2 éléments ne sont pas égales (on utilise donc l'opérateur
              de comparaison d'inégalité != */
               {
                alert("Les deux "+fieldalias+" ne condordent pas")
            // toujours la boite d'alerte
                element1.select()
            // Et la on utilise la méthode select qui permet de selectionner la valeur écrite.
               }

               else
               passed=true
               return passed
             }
        </script>

    </head>
    
    <style type="text/css">
        h1 {
           text-align: center;
        }
        p {
            margin-bottom: 25px;
        }
        
        .info {
            margin-right: auto;
            margin-left: auto;
            margin-bottom: 20px;
            padding-right: auto;
            padding-left: auto;
            padding-bottom: 10px;
            border: 1px black solid;
            width: 1000px;
        }
        
        table {
            margin-right: auto;
            margin-left: auto;
        }
        td {
            padding: 5px;
            margin-right: 20px;
        }
        .labelTable {
            text-align: right;
        }
        
        body {
            padding-top: 70px;
            padding-bottom: 40px;
        }
        
        .listBox {
            padding-left: 100px;
            padding-right: 200px;
            padding-top: 10px;
            padding-bottom: 20px;
            width: 500px;
        }
        
        .div1, .div2 {
            border: 1px solid black;
            width: 500px;
            /*float: left;*/
            /*display: inline-block*/
        }
        
        .div2 {
            /*float: right;*/
        }
        
        
        
    </style>
    
    
    <body>
        <form method="post" action="traitement_inscription.php" onSubmit="return verify(this.pass, this.pass2)">
        <h1>Inscription</h1>
        <div class="container">
            
            <div class="well" >

                <!-- Informations du compte -->
                <h3>Informations du compte</h3>
                <table>
                    <tr>
                        <td>Email entreprise</td><td><input type="email" class="form-control" name="Email_entreprise" required /></td>

                    </tr>
                    <tr>
                        <td>Mot de passe</td><td><input type="password" class="form-control" name="pass" required /></td>
                        <td>Confirmation du mot de passe</td><td><input type="password" class="form-control" name="pass2" required /></td>
                    </tr>
                </table>
            
            </div>
        </div>
        
        <div class="container">
            
            <div class="well" >
        
                <!-- Informations professionnelles -->
                <h3>Informations professionnelles</h3>
                <table>
                    <tr>
                        <td class="labelTable">Numéro de Siret (14 chiffres) </td><td><input type="number" class="form-control" name="Num_siret" required /></td>
                        <td class="labelTable">Raison sociale</td><td><input type="text" class="form-control" name="Raison_sociale" required /></td>
                    </tr>
                    <tr>
                        <td class="labelTable">Numéro téléphone</td><td><input type="tel" class="form-control" name="Num_tel_entreprise" required /></td>
                    </tr>
                    <tr>
                        <td class="labelTable">Numéro de rue</td><td><input type="text" class="form-control" name="Num_rue_entreprise" required /></td>
                        <td class="labelTable">Rue</td><td><input type="text" class="form-control" name="Nom_rue_entreprise" required /></td>
                    </tr>
                    <tr>
                        <td class="labelTable">Ville</td><td><input type="text" class="form-control" name="Ville_entreprise" required /></td>
                        <td class="labelTable">Code Postal</td><td><input type="text" class="form-control" name="CP_entreprise" required /></td>
                    </tr>
                </table>
            </div>
        </div>
            

        <div class="container">
            
            <div class="well" >
                <!-- Contrat -->
                <p><input type="radio" name="contrat" value="cotisation" onclick="met(this,'div1','div2');" checked /> Cotisation
                    <input type="radio" name="contrat" value="prestation" onclick="met(this,'div2','div1');" /> Prestation</p>

                
                <label>Périodicité </label>
                <p class="listBox"> <select name="periodicite" class="form-control comboPeriodicite">
                    <option selected value="mois"> Mois
                    <option value="trimestre"> Trimestre
                    <option value="semestre"> Semestre
                    <option value="annee"> Année
                </select></p>
                
                <table class="div1">
                    
                <p><input type="radio" name="cotisation" value="meilleurSalaire" checked="checked" /> Indexé meilleur salaire </p>

                <p><input type="radio" name="cotisation" value="salaireMoyen"/> Salaire moyen carrière </p>
                
                </table>



                <table class="div2">
                <input type="radio" name="cotisation" value="annuelle" /> Cotisation annuelle
                <p class="listBox"> <select name="montantCotisation" class="form-control comboMontantCotisation">
                    <option selected value="600"> 600
                    <option value="1200"> 1 200
                    <option value="2400"> 2 400
                    <option value="6000"> 6 000
                    <option value="12000"> 12 000
                </select></p>

                <input type="radio" name="cotisation" value="choix" /> Autre montant <p class="listBox"><input type="text" class="form-control" name="autreMontantCotisation" /></p>

                </table>
            </div>
            <input type="submit" name="btnValider" value="Valider" />
        </div>
        </form>
    </body>
</html>
