
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8"/>
        <meta name="description" content="Projet Assuramort"/>
        <meta name="keywords" content="Assuramort, Assurance"/>
        <meta name="author" content="Geeks"/>
        <title>Assuramort</title>
        <link rel="icon" type="image/png" href="images/logo.png"/>
        <link rel="stylesheet" type="text/css" media="screen" href="css/style.css"/>
    </head>
    <body>
        <div id="barfixed">
            <a href="index.php?content=accueil"><img src="images/home.png" alt="Retour accueil" float:right/></a>
            <p>Assuramort</p>
        </div>
        <H1><center><font color="red"> Contrat </font></center></h1>
       <div id="container">
             Le présent Accord de Souscription à l'assurance <strong>Assuramort</strong> est un document à valeur juridique qui explique vos droits et obligations en tant que Souscripteur. Veuillez le lire avec attention.

        Si votre résidence principale se trouve dans l'un des pays membres de l'Union européenne (un « Souscripteur de l'UE »), votre interlocuteur est Valve S.a.r.l. (« Valve UE »). Pour tous les autres Souscripteurs, votre interlocuteur est Valve Corporation (« Valve E-U »). Sauf mention contraire indiquée au moment de l'achat (par exemple en cas d'achats auprès d'un autre Souscripteur via le Marché de Souscriptions), tous vos achats sont effectués auprès de l'interlocuteur Valve dont vous dépendez. Sauf mention contraire dans les présentes, toute référence à « Valve » dans le présent Accord signifie Valve UE si vous êtes Souscripteur de l'UE et Valve E-U si vous êtes Souscripteur dans le reste du monde. Toute référence à « Valve » dans les Règles d'utilisation ou Conditions de Souscription, si et tel que le contexte l'exige, signifie Valve UE si vous êtes Souscripteur de l'UE et Valve E-U s'il en est autrement.

        LA SECTION 12 CONTIENT UN ACCORD D'ARBITRAGE OBLIGATOIRE ET UNE RENONCIATION AUX RECOURS COLLECTIFS. CELA AFFECTE VOS DROITS JURIDIQUES. VEUILLEZ LA LIRE. SI VOUS RESIDEZ HORS DES ETATS-UNIS, TOUT OU PARTIE DE LA SECTION 12 PEUT NE PAS S'APPLIQUER. PAR EXEMPLE, SI VOUS VIVEZ DANS UN PAYS MEMBRE DE L'UNION EUROPEENNE, LA SECTION 12 NE S'APPLIQUE PAS A VOUS.

        <h3><font color="red">1. INSCRIPTION EN TANT QUE SOUSCRIPTEUR, COTISATIONS</font></h3>
<br/>
Steam est un service en ligne proposé par Valve.

Vous devenez souscripteur de Steam (« Souscripteur ») en installant le logiciel client Steam et en remplissant la fiche d'inscription Steam. Cet Accord entre en vigueur dès l'acceptation des présentes conditions.

En tant que Souscripteur, vous pouvez bénéficier de certains services, logiciels et contenus proposés aux Souscripteurs. Le logiciel client Steam et les autres logiciels, contenus et mises à jour que vous téléchargez ou auxquels vous accédez par le biais de Steam, y compris et sans s'y limiter les jeux vidéo et contenus de jeu Valve ou tiers, ainsi que les objets virtuels achetés sur un Marché de Souscriptions Steam, sont appelés « Logiciels » dans le présent Accord ; les droits d'accès et/ou d'utilisation des services, Logiciels et/ou contenus accessibles par l'intermédiaire de Steam sont appelés « Souscriptions » dans les présentes.

Chaque Souscription vous permet d'accéder à différents services, Logiciels et/ou contenus. Des conditions supplémentaires peuvent s'appliquer à certaines Souscriptions (« Conditions de Souscription ») (par exemple, un accord de licence d'utilisateur final propre à un jeu spécifique, ou des conditions d'utilisation spécifiques à un produit ou une fonction de Steam). Par ailleurs, des conditions supplémentaires (par exemple, des procédures de paiement et de facturation) peuvent être publiées sur http://www.steampowered.com ou au sein du service Steam (des « Règles d'utilisation »). Les Règles d'utilisation incluent les Règles de bonne conduite en ligne Steam, disponibles à l'adresse http://steampowered.com/index.php?area=online_conduct. Les Conditions de Souscription, Règles d'utilisation et la Charte de protection de la vie privée Valve (disponible à l'adresse http://www.valvesoftware.com/privacy.htm) vous engagent dès lors que vous avez manifesté votre accord vis-à-vis de ces documents ou du présent Accord, ou que vous devenez lié à ces documents comme décrit à la Section 9 (Amendements au présent Accord).

Une fois la procédure d'inscription à Steam effectuée, vous créez un compte Steam (un « Compte »). Votre Compte peut également inclure les informations de facturation que vous communiquez à Valve lors de l'achat de Souscriptions. Toute activité sur votre compte et la sécurité de votre système informatique sont de votre seule et entière responsabilité. Vous n'êtes pas autorisé à dévoiler, partager ou permettre à des tiers d'utiliser votre mot de passe ou votre Compte. Vous acceptez être personnellement responsable de l'utilisation qui est faite de votre Compte et de toutes les communications et activités sur Steam résultant de l'utilisation de votre nom d'utilisateur et de votre mot de passe. Vous n'êtes pas autorisé à vendre ou facturer le droit d'utilisation de votre Compte à des tiers, ni à le transférer. De la même manière, vous n'est pas autorisé à vendre ou facturer le droit d'utiliser des Souscriptions, ni à les transférer, sauf autorisation expresse dans les présentes (y compris les Conditions de Souscription et Règles d'utilisation).

<h3><font color="red">2.INSCRIPTION EN TANT QUE SOUSCRIPTEUR, PRESTATIONS </font></h3>
<br/>

<strong>A. Licence générale d'utilisation de Logiciels</strong>
<br/>
Steam et vos Souscriptions nécessitent le téléchargement et l'installation automatiques de Logiciels sur votre ordinateur. Par la présente, Valve vous octroie, et vous acceptez, une licence limitée, résiliable et non exclusive d'utilisation des Logiciels à titre privé et non commercial (sauf les cas où une utilisation commerciale est autorisée par les présentes ou les Conditions de Souscription applicables) conformément au présent Accord, comprenant les Conditions de Souscription. Les Logiciels sont concédés sous licence, et non vendus. Votre licence ne vous confère aucun droit ni titre de propriété sur les Logiciels. Pour utiliser les Logiciels, vous devez être en possession d'un Compte Steam et vous pouvez être tenu d'exécuter le client Steam et d'être connecté à Internet pendant toute la durée d'utilisation.

Pour des raisons liées notamment à la sécurité du système, à la stabilité et à l'interopérabilité multijoueur, Steam peut être amené à automatiquement mettre à jour, précharger, créer de nouvelles versions des Logiciels ou à les améliorer, et par conséquent, la configuration système requise pour l'utilisation des Logiciels peut évoluer dans le temps. Vous acceptez lesdites mises à jour automatiques. Vous reconnaissez par ailleurs que le présent Accord (y compris les Conditions de Souscription applicables) ne vous confère aucun droit à exiger des mises à jour, nouvelles versions ou autres améliorations apportées aux Logiciels. Valve décide de fournir lesdites mises à jour, etc. à son entière discrétion.
<br/>
<strong>B. Licence d'utilisation de Logiciels bêta</strong>
<br/>
Valve peut à tout moment mettre à votre disposition via Steam des logiciels pas encore commercialisés (des « Logiciels bêta »). Vous n'êtes pas tenu d'utiliser les Logiciels bêta, mais si Valve vous en propose, vous pouvez choisir de les utiliser conformément aux conditions ci-après. Les Logiciels bêta sont assimilés à des Logiciels et chaque copie d'un Logiciel bêta fournie est réputée constituer une Souscription audit Logiciel, les dispositions suivantes étant spécifiques aux Logiciels bêta :

    Votre droit d'utilisation d'un Logiciel bêta peut être limité dans le temps et soumis à des Conditions de Souscription supplémentaires ;
    Valve et les sociétés affiliées à Valve peuvent vous demander ou exiger que vous fournissiez des suggestions, commentaires ou informations concernant votre utilisation d'un Logiciel bêta, qui seront considérées comme du Contenu généré par l'utilisateur conformément à la Section 6 (Contenu généré par l'utilisateur) ci-dessous ; et
    Outre les renonciations et limitations de responsabilité relatives à tous les Logiciels conformément à la Section 7 (Décharges, limitations de responsabilité et absence de garanties) ci-dessous, vous reconnaissez en particulier que les Logiciels bêta ne sont pas des versions définitives et qu'ils peuvent entraîner des incompatibilités ou endommager votre ordinateur, vos données et/ou vos logiciels.
       </div>
    </body>
</html>