<?php
    require_once('base.php');
    $bdd = connect_db();
    if(isset($_SESSION['id'])){
        $entiteencours = $_SESSION['id'];
    }
    else  $entiteencours = 1;
    
    
    $reqEmploye = $bdd->prepare("SELECT * From Salarie WHERE Email_salarie = '".$_SESSION['login']."';");
    $reqEmploye->execute() or die (print_r($reqEmploye->errorInfo()));
    $infoEmploye = $reqEmploye->fetch(PDO::FETCH_ASSOC);
    
    $_SESSION['id'] = $infoEmploye['ID_salarie'];
    $entiteencours = $_SESSION['id'];
    
    $reqCotisations = $bdd->prepare("SELECT * From Cotisation WHERE ID_salarie = '".$_SESSION['id']."' order by Date_cotisation DESC;");
    $reqCotisations->execute() or die (print_r($reqCotisations->errorInfo()));
    $listeCotisations = $reqCotisations;
    
?>
<center>
    <h2>Liste des cotisations précédentes</h2>
    <table>
        <tr>
            <th>Date</th>
            <th>Montant</th>
        </tr>
        <?php foreach ($listeCotisations as $uneCotisation) {
            echo "<tr>"
            . "<td>".$uneCotisation["Date_cotisation"]."</td>"
            . "<td>".$uneCotisation["Montant_cotisation"]."</td>"
            . "</tr>";
        }
        ?>

    </table>
    
</center>