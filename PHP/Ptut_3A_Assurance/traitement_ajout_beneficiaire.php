<?php
session_start();
    require_once('base.php');
    $bdd = connect_db();
    if(isset($_SESSION['id'])){
        $entiteencours = $_SESSION['id'];
    }

    else  $entiteencours = 1;
    
    
    $id_salarie = $_SESSION["id"];
    $nom_bene = $_POST["nom_bene"];
    $prenom_bene = $_POST["prenom_bene"];
    $date_naissance_bene = $_POST["date_naissance_bene"];
    $lien_parente_bene = $_POST["lien_parente_bene"];
    $pourcentage_bene = $_POST["pourcentage_bene"];
	
	$reqSommePourcentage = $bdd->prepare("select SUM(Pourcentage_beneficiaire) as 'Somme_pourcentage' from Beneficiaire where ID_salarie=".$id_salarie);
	$reqSommePourcentage->execute() or die (print_r($reqSommePourcentage->errorInfo()));
	$resSommePourcentage = $reqSommePourcentage->fetch(PDO::FETCH_ASSOC);
	$sommePour = $resSommePourcentage["Somme_pourcentage"];
	
	
	if ($date_naissance_bene == "")
		$erreur = "Veuillez renseigner la date de naissance du bénéficiaire.";
	if ($pourcentage_bene == "")
		$erreur = "Veuillez renseigner le pourcentage alloué au bénéficiaire.";
	if ($pourcentage_bene <= 0)
		$erreur = "Le pourcentage alloué au bénéficiaire doit être supérieur à 0.";
	
	if ((($sommePour == null) && ($pourcentage_bene <= 1)) || ($pourcentage_bene + $sommePour <= 1)) {
		$reqAjoutBene = $bdd->prepare("insert into Beneficiaire (Nom_beneficiaire, Prenom_beneficiaire, Date_naissance_beneficiaire, Lien_parente_beneficiaire, Pourcentage_beneficiaire, ID_salarie) "
            ."values ('$nom_bene', '$prenom_bene', '$date_naissance_bene', '$lien_parente_bene', $pourcentage_bene, $id_salarie)");
		$message = "Le bénéficiaire a été ajouté avec succès.";
	} else $erreur = "La somme du pourcentage des bénéficiaires ne doit pas dépasser 1.";
	
	
	if (isset($erreur))
		$_SESSION["erreur"] = $erreur;
	else {
		$reqAjoutBene->execute() or die (print_r($reqAjoutBene->errorInfo()));
		$_SESSION["message"] = $message;
		
	}
    
    
    header('Location: http://localhost/ptut_3a_assurance/php/ptut_3a_assurance/index.php?content=employe');