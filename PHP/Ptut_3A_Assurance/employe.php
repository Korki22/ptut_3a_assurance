<?php
    require_once('base.php');
    $bdd = connect_db();
    if(isset($_SESSION['id'])){
        $entiteencours = $_SESSION['id'];
    }
    else  $entiteencours = 1;

    $reqEmploye = $bdd->prepare("SELECT * From Salarie WHERE Email_salarie = '".$_SESSION['login']."';");
    $reqEmploye->execute() or die (print_r($reqEmploye->errorInfo()));
    $infoEmploye = $reqEmploye->fetch(PDO::FETCH_ASSOC);
    
    $_SESSION['id'] = $infoEmploye['ID_salarie'];
    $entiteencours = $_SESSION['id'];

    $reqPostes = $bdd->prepare("SELECT * From Poste order by ID_poste;");
    $reqPostes->execute() or die (print_r($reqPostes->errorInfo()));
    $postes = $reqPostes;

    $reqPosteEmp = $bdd->prepare("SELECT * From Salaire join Salarie on Salarie.id_salarie = Salaire.id_salarie "
            . "join Poste p on p.id_poste = Salaire.id_poste WHERE Email_salarie = '".$entiteencours."' and Date_fin_salaire = NULL;");
    $reqPosteEmp->execute() or die (print_r($reqPosteEmp->errorInfo()));
    $posteEmp= $reqPosteEmp->fetch(PDO::FETCH_ASSOC);

    $reqSituFami = $bdd->prepare("SELECT * From Situation_familiale order by ID_situation_familiale;");
    $reqSituFami->execute() or die (print_r($reqPostes->errorInfo()));
    $situFami = $reqSituFami;
    
    $reqModeVers = $bdd->prepare("SELECT * From Mode_de_versement order by ID_mode_versement;");
    $reqModeVers->execute() or die (print_r($reqModeVers->errorInfo()));
    $modesVers = $reqModeVers;
	
	if ($infoEmploye['ID_mode_versement'] != null)
	{
		$reqNomModeVersChoix = $bdd->prepare("SELECT Nom_mode_versement from Mode_de_versement where ID_mode_versement = ".$infoEmploye["ID_mode_versement"].";");
		$reqNomModeVersChoix->execute() or die (print_r($reqNomModeVersChoix->errorInfo()));
		$nomModeVersChoix = $reqNomModeVersChoix->fetch(PDO::FETCH_ASSOC);
	} 
    
    $reqBeneficiaires = $bdd->prepare("SELECT * From Beneficiaire WHERE ID_salarie = ".$entiteencours." order by ID_beneficiaire;");
    $reqBeneficiaires->execute() or die (print_r($reqBeneficiaires->errorInfo()));
    $beneficiaires = $reqBeneficiaires;
	
	if (isset($_SESSION["erreur"])) {
		if ($_SESSION["erreur"] != "") {
			$erreur = $_SESSION["erreur"];
			$_SESSION["erreur"] = "";
		}
	}
	if (isset($_SESSION["message"])) {
		if ($_SESSION["message"] != "") {
			$message = $_SESSION["message"];
			$_SESSION["message"] = "";
		}
	}
	
?>    
    <?php
	if (isset($_SESSION['isConnected']) and ($_SESSION['isConnected'] == 1)) 
    {
		if (isset($erreur)) {
			?> <div class="alert alert-danger" role="alert" style="margin:50px 100px 0px 100px;"><?= $erreur ?></div> <?php
		}
		if (isset($message)) {
			?> <div class="alert alert-success" role="alert" style="margin:50px 100px 0px 100px;;"><?= $message ?></div> <?php
		}
	?>
	
                <div><center>
                        <form method="post" action="http://localhost/ptut_3a_assurance/php/ptut_3a_assurance/traitement_emp.php" >
                            <h1>Inscription</h1>

                            <div class="info">
                            <!-- Informations du compte -->
                            <h3>Informations du compte</h3>
                            <table>
                                    <tr>
                                            <td>Email employé</td><td><input type="text" name="Email_salarie" value="<?= $_SESSION['login'] ?>" /></td>

                                    </tr>
                                    <tr>
                                            <td>Mot de passe</td><td><input type="password" name="Pass_salarie" /></td>
                                            <td>Confirmation du mot de passe</td><td><input type="password" name="Pass_salarie2"  /></td>
                                    </tr>
                            </table>
                            </div>


                            <div class="info">
                            <!-- Informations professionnelles -->
                            <h3>Informations professionnelles</h3>
                            <table>
                                    <tr>
                                            <td class="labelTable">Nom</td><td><input type="text" name="Nom_salarie" value="<?= $infoEmploye['Nom_salarie'] ?>" /></td>
                                            <td class="labelTable">Prénom</td><td><input type="text" name="Prenom_salarie" value="<?= $infoEmploye['Prenom_salarie'] ?>" /></td>
                                    </tr>
                                    <tr>
                                            <td class="labelTable">Date de naissance</td><td><input type="date" name="Date_naissance_salarie" value="<?= $infoEmploye['Date_naissance_salarie'] ?>" /></td>
                                            <td class="labelTable">Poste</td>
                                                    <td>
                                                            <?php
															if ($posteEmp['ID_poste'] == "") echo "Retraité";
                                                            else foreach ($postes as $poste) {
                                                                    if ($posteEmp['ID_poste'] == $poste['ID_poste']) echo "poste : ".$poste['Intitule_poste'];
                                                            } ?>
                                                    </td>
                                    </tr>
                                    <tr>
                                            <td class="labelTable">Situation familiale</td>
                                            <td><select name="Id_situation_familiale" class="comboSituationFamiliale">
                                                    <?php
                                                    foreach ($situFami as $uneSituFami) {
                                                            if ($infoEmploye['ID_situation_familiale'] == $uneSituFami['ID_situation_familiale']) $selected = "selected";
                                                            else $selected = "";
                                                            echo "<option ".$selected." value='".$uneSituFami['ID_situation_familiale']."' > ".$uneSituFami['Situation_familiale'];
                                                    } ?>
                                            </select></td>                    
                                            <td class="labelTable">Nombre d'enfants</td><td><input name="Nombre_enfants_salarie" type="number" min="0" max="20" step="1" value="<?= $infoEmploye['Nombre_enfants_salarie'] ?>" required="true" /></td>
                                    </tr>
                                    <tr>
                                            <td class="labelTable">Numéro téléphone</td><td><input type="tel" pattern="^\+?\s*(\d+\s?){8,}$" name="Num_tel_salarie" value="<?= $infoEmploye['Num_tel_salarie'] ?>" /></td>
                                            <td class="labelTable">Rue</td><td><input type="text" name="Nom_rue_salarie" value="<?= $infoEmploye['Nom_rue_salarie'] ?>" /></td>
                                    </tr>
                                    <tr>
                                            <td class="labelTable">Ville</td><td><input type="text" name="Ville_salarie" value="<?= $infoEmploye['Ville_salarie'] ?>" /></td>
                                            <td class="labelTable">Code Postal</td><td><input type="text" name="CP_salarie" pattern="^\+?\s*(\d+\s?){5,}$" value="<?= $infoEmploye['CP_salarie'] ?>" /></td>
                                    </tr>
                            </table>
                            </div>

                            <div class="info">

                                    <h3>Informations sur le conjoint</h3>
                                    <table>
                                            <tr>
                                                    <td class="labelTable">Nom</td><td><input type="text" name="Nom_conjoint_salarie" value="<?= $infoEmploye['Nom_conjoint_salarie'] ?>" /></td>
                                                    <td class="labelTable">Prénom</td><td><input type="text" name="Prenom_conjoint_salarie" value="<?= $infoEmploye['Prenom_conjoint_salarie'] ?>" /></td>
                                            </tr>
                                            <tr>
                                                    <td class="labelTable">Date de naissance</td><td><input type="date" name="Date_naissance_conjoint_salarie" value="<?= $infoEmploye['Date_naissance_conjoint_salarie'] ?>" /></td>
                                            </tr>

                                    </table>

                            </div>

                            <div class="info">

                                <h3>Contrat</h3>

                                <p>Choix mode de versement : </p>
                                <?php
								if ($infoEmploye['ID_mode_versement'] == null)
								{
									foreach ($modesVers as $unModeVers) {
										if ($infoEmploye['ID_mode_versement'] == $unModeVers['ID_mode_versement']) $selected = "checked";
										else $selected = "";
										echo "<INPUT type='radio' name='Mode_versement_salarie' value=".$unModeVers['ID_mode_versement']." ".$selected." >".$unModeVers['Nom_mode_versement'];
									}
								} else echo $nomModeVersChoix["Nom_mode_versement"];
                                ?>

                            </div>
							
							<div style="margin-top:40px;margin-bottom:60px;"><input type="submit" value="Modifier mes données" class="btn btn-default"></div>
                        
                        </form>
                        
                        <h3>Bénéficiaires : </h3>
                        <table style="margin-bottom:50px;">
                            <tr>
                                <th>Nom</th>
                                <th>Prénom</th>
                                <th>Date de naissance</th>
                                <th>Lien de parenté</th>
                                <th>Pourcentage</th>
                                <th>Suppression</th>
                            </tr>
                        <?php foreach ($beneficiaires as $unBeneficiaire) {
                            echo "<tr>
                                <td>".$unBeneficiaire["Nom_beneficiaire"]."</td>
                                <td>".$unBeneficiaire["Prenom_beneficiaire"]."</td>
                                <td>".$unBeneficiaire["Date_naissance_beneficiaire"]."</td>
                                <td>".$unBeneficiaire["Lien_parente_beneficiaire"]."</td>
                                <td>".$unBeneficiaire["Pourcentage_beneficiaire"]."</td>"; ?>
                                <td><input type="button" class="btn btn-danger" onClick="window.location.href='http://localhost/ptut_3a_assurance/php/ptut_3a_assurance/traitement_suppression_beneficiaire.php?id_bene=<?= $unBeneficiaire["ID_beneficiaire"] ?>'" value="X"></td>
                            </tr>
                            
                            
                        <?php } ?>
                            <form method="post" action="http://localhost/ptut_3a_assurance/php/ptut_3a_assurance/traitement_ajout_beneficiaire.php"
                            <tr>
                                <td><input type='text' name="nom_bene" width='100%' /></td>
                                <td><input type='text' name="prenom_bene" width='100%' /></td>
                                <td><input type='date' name="date_naissance_bene" width='100%' /></td>
                                <td><input type='text' name="lien_parente_bene" width='100%' /></td>
                                <td><input type='number' min="0" max="1" step="0.1" name="pourcentage_bene" width='100%' /></td>
                                <td><INPUT TYPE="submit" class="btn btn-info" VALUE="+"></td>
                            </tr>
                        </table>
                        
                        
			
    </center></div>
	<?php
	}
	?>
</html>