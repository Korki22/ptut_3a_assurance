<?php
    require_once('base.php');
    $bdd = connect_db();
    if(isset($_SESSION['id'])){
        $entiteencours = $_SESSION['id'];
    }

    else  $entiteencours = 1;

?>

        <div id="container">
            <div class="blockgauche">
                <h3>Fonctionalités</h3>
                     <ol class="list2">
                        <li><a href="javascript:void(0);" onclick="javascript:window.open('importcsv.php','popup','resizable=no,scrollbars=yes,location=no,width=500,height=500,top=200,left=750');">Importer des salariés</a></li>
                        <li><a href="#">Modifier le statut d'un salarié</a></li>
                        <li><a href="#">Verifier les termes du contrat</a></li>
                        <li><a href="#">Contacter l'Assureur</a></li>
                        <li><a href="#">Editer l'Avenant</a></li>
                    </ol>
            </div>
            <div class="blockdroite">
                <?php
                    // Si l'utilisateur courant est une entreprise
                    if (isset($_SESSION['isConnected']) and ($_SESSION['isConnected'] == 2))
                    {

                                echo "<h2> Liste de vos salariés </h2>";

                                // Requete qui selectionne toutes les infos concernant les salarie
                                    $reqsal = $bdd->prepare("SELECT * FROM Salarie Inner join Entreprise on Salarie.ID_Entreprise = Entreprise.ID_Entreprise WHERE Salarie.id_entreprise = $entiteencours");   
                                    $reqsal->execute() or die (print_r($reqsal->errorInfo()));
                                    $taille = $reqsal-> rowCount();
                                    if ($taille == 0){
                                        echo "Aucun salarié n'est affecté à votre entreprise";
                                        echo "<br/>";
                                        echo "Peut-être voulez vous en ajouter un ?";
                                        echo "<br/>";
                                        ?>
                                        <a href="javascript:void(0);" onclick="javascript:window.open('importcsv.php','popup','resizable=no,scrollbars=yes,location=no,width=500,height=500,top=200,left=750');">Importer des salariés</a>
                                        <?php
                                        
                                    }
                                    else { 
 
                                    // L'entête du tableau
                                    ECHO "</br>";
                                    echo "<table cellspacing='1' class='tablesorter' style='margin-left:100px;'>";
                                    echo "<tr>";
                                    echo "<td class='prenom'>Prenom</td>";
                                    echo "<td class='nom'>Nom</td>";
                                    echo "<td class='datenaiss'>Age</td>";
                                    echo "<td class='lieunaiss'>Cotisations</td>";
                                    echo "</tr>";
                                    while ($ligne = $reqsal->fetch(PDO::FETCH_ASSOC)) {
                                        if($ligne['Age_debut_differe'] == "") $ligne['Age_debut_differe'] = 'N/A';
                                    // tant que l'on trouve des sal, on les ajoutes au tableau
                                    echo "<tr>";
                                    //  Cette première ligne, par exemple, ajoute le prenom du salarie et fais un lien vers celui-ci
                                    echo "<td class='prenom'><a href='index.php?content=employe'>".$ligne['Nom_salarie']."</a> </td>";
                                    echo "<td class='nom'><a href='index.php?content=employe.php?id=".$ligne['ID_salarie']."'>".$ligne['Prenom_salarie']."</a></td>";
                                    echo "<td class='age'><a href='index.php?content=employe.php?id=".$ligne['ID_salarie']."'>".$ligne['Age_debut_differe']."</a>  </td>";
                                    echo "<td class='cotisation'><a href='index.php?content=employe.php?id=".$ligne['ID_salarie']."'>".$ligne['Age_debut_differe']."</a> </td>";
                                    echo "</tr>";                                 
                                     }
                                    }
                    } // FIN IF entreprise
                    // Si c'est un salarié
                    else
                        {if (isset($_SESSION['isConnected']) and ($_SESSION['isConnected'] == 1)) 
                            {

                        $requete =$bdd->prepare("SELECT ID_Entreprise FROM Salarie WHERE Nom_Salarie = $entiteencours");
                        $requete->execute() or die (print_r($requete->errorInfo()));
                        $lignequelsalarie = $requete->fetch(PDO::FETCH_ASSOC);
                        $monsalarie = $lignequelsalarie['ID_Entreprise'];

                        $reqinfoent = $bdd->prepare("SELECT * FROM Entreprise Inner join Salarie on Entreprise.ID_Entreprise = Salarie.ID_Entreprise 
                                    and Entreprise.ID_Entreprise = $monsalarie");
                        $reqinfoent->execute() or die (print_r($reqinfoent->errorInfo()));
                        $ligneent = $reqinfoent->fetch(PDO::FETCH_ASSOC);
                        ?>
                        <div id="container">
                            <h2>Informations juridiques</h2>
                            <table cellspacing='0'> <!-- cellspacing='0' is important, must stay -->
                                <thead>
                                    <tr>
                                        <th>Champs</th>
                                        <th>Valeur</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr><td>Raison sociale/td><td><?php echo $ligneent['Raison_sociale']?></td></tr>
                                    <tr class="even"><td>Addresse</td><td><?php echo $ligneent['Nom_rue_entreprise']?></td></tr>
                                    <tr><td>Ville</td><td><?php echo $ligneent['Ville_entreprise']?></td></tr>
                                    <tr class="even"> <td>Code Postale</td><td><?php echo $ligneent['CP_entreprise']?></td></tr>
                                    <tr><td>Email</td><td><?php echo $ligneent['Email_entreprise']?></td></tr>
                                    <tr class="even"><td>Telephone</td><td><?php echo $ligneent['Num_tel_entreprise']?></td></tr>
                                    <tr><td>Date adhésion</td><td><?php echo $ligneent['Date_adhesion']?></td></tr>
                                </tbody>
                            </table>
                        </div>
                        <?php
                        } // fin if de else
                    } // fin else

                ?>

            </div>
        </div>

 