<?php
    session_start();
    if (!isset($_SESSION['isConnected'])){
        header('Location: /ptut_3a_assurance/php/ptut_3a_assurance/testcon.php');
    }
?>

<!DOCTYPE html>

<html>
    <?php
        $content = "";

        if(isset($_GET['content']))
        {
            $content = $_GET['content'];
        }
        else
        {
            $content = "accueil";
        }

        $page = $content.".php";
    ?>

    <head>
        <meta charset="utf-8"/>
        <meta name="description" content="Projet Assuramort"/>
        <meta name="keywords" content="Assuramort, Assurance"/>
        <meta name="author" content="Geeks"/>
        <title>Assuramort</title>
        <link rel="icon" type="image/png" href="images/logo.png"/>
        <link rel="stylesheet" type="text/css" media="screen" href="css/style.css"/>
        <link href="Bootstrap/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div id="barfixed">
            <a href="index.php?content=accueil"><img src="images/home.png" alt="Retour accueil" float:right/></a>
            <p>Assuramort</p>
        </div>
        <div id="wrap">
            <header>
                <div class="clear"></div>    
                <nav class="box-shadow-services">
                    <div>
                        <ul class="menu-services">
                            <li class="home-page"><a href="index.php?content=accueil"><span></span></a></li>
                            <?php
                                if (isset($_SESSION['isConnected']) and ($_SESSION['isConnected'] == 0)) { ?>
                                    <li><a href="index.php?content=contrat">Contrat</a></li>
                                    <li><a href="infoEnt.php">Inscription</a></li>
                                <?php }
                                else {
                                    if (isset($_SESSION['isConnected']) and ($_SESSION['isConnected'] == 1)) { ?>
                                    <li><a href="index.php?content=employe">Informations personnelles</a></li>
                                    <li><a href="index.php?content=cotisations">Cotisations</a></li>
                                    <li><a href="index.php?content=traitement_deco">Déconnexion</a></li>
                                    <?php }
                                    else {
                                        if (isset($_SESSION['isConnected']) and ($_SESSION['isConnected'] == 2)) { ?>
                                            <li><a href="index.php?content=accueilent">Panneau de configuration</a></li>
                                            <li><a href="#">Salariés</a></li>
                                            <li><a href="index.php?content=traitement_deco">Déconnexion</a></li>
                                        <?php }
                                    }
                                }
                            ?>
                        </ul>
                        <div class="social-icons">
                            <?php
                                if (isset($_SESSION['isConnected']) and ($_SESSION['isConnected'] == 0)) { ?>
                                    <span><a href="javascript:void(0);" onclick="javascript:window.open('test.php','popup','resizable=no,scrollbars=yes,location=no,width=500,height=500,top=200,left=750');">Connexion</a></span>
                                <?php }
                                else { ?>
                                    <span>Bonjour <?php echo $_SESSION['nom'] ?></span>
                                <?php }
                            ?>
                        </div>
                        <div class="clear"></div>
                    </div>
                </nav>
            </header>
        </div>
        <?php
            include $page;
        ?>
    </body>
</html>