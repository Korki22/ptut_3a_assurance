<!DOCTYPE html>

<html>
	<head> 
		<meta charset="utf-8"/>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta name="description" content="Projet Assuramort"/>
        <meta name="keywords" content="Assuramort, Assurance"/>
        <meta name="author" content="Geeks"/>
        <title>Login</title>
        <link rel="icon" type="image/png" href="images/logo.png"/>
        <link rel="stylesheet" type="text/css" href="style.css">
		<style type="text/css">
			html {	
				height: 100%;
				background: url('http://thecodeplayer.com/uploads/media/gs.png');
				/*background = gradient + image pattern combo*/
				background: linear-gradient(rgba(196, 102, 0, 0.2), rgba(155, 89, 182, 0.2)), url('http://thecodeplayer.com/uploads/media/gs.png');
			}

			#msform {
				width: 400px;
				margin: 50px auto;
				text-align: center;
				position: relative;
			}

			#msform fieldset {
				background: white;
				border: 0 none;
				border-radius: 3px;
				box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4);
				padding: 20px 30px;
				
				box-sizing: border-box;
				width: 80%;
				margin: 0 10%;
				/*stacking fieldsets above each other*/
				position: absolute;
			}

			/*inputs*/
			#msform input, #msform textarea {
				padding: 15px;
				border: 1px solid #ccc;
				border-radius: 3px;
				margin-bottom: 10px;
				width: 100%;
				box-sizing: border-box;
				font-family: montserrat;
				color: #2C3E50;
				font-size: 13px;
			}

			/*buttons*/
			#msform .action-button {
				width: 100px;
				background: #27AE60;
				font-weight: bold;
				color: white;
				border: 0 none;
				border-radius: 1px;
				cursor: pointer;
				padding: 10px 5px;
				margin: 10px 5px;
			}

			#msform .action-button:hover, #msform .action-button:focus {
				box-shadow: 0 0 0 2px white, 0 0 0 3px #27AE60;
			}

			/*headings*/
			.fs-title {
				font-size: 15px;
				text-transform: uppercase;
				color: #2C3E50;
				margin-bottom: 19px;
			}

			/*buttons*/
			#msform .action-buttons {
				width: 150px;			
				font-weight: bold;
				color: white;
				border: 0 none;
				border-radius: 1px;
				cursor: pointer;
				padding: 10px 5px;
				margin: 10px 5px;
			}

		</style>
	</head>
	<?php

	if(isset($_GET['success'])){
		switch ($_GET['success']) {
	    	case 1:
	        	$texte = '<p style="color: green; font-size: 16px;">Fichier importé !</p>';
	        	break;
	    	case 2:
	        	$texte = '<p style="color: red; font-size: 16px;">Erreur de chargement !!!</p>';
	        	break;
	    	case 3:
	        	$texte = '<p style="color: red; font-size: 16px;">Fichier trop gros !!!</p>';
	        	break;
	     	case 4:
	        	$texte = '<p style="color: red; font-size: 16px;">Extension non csv !!!</p>';
	        	break;
	        case 5:
	        	$texte = '<p style="color: red; font-size: 16px;">Aucun fichier choisi !!!</p>';
	        	break;
		}
	}
	else {
		$texte = "Importer votre liste de salarié";
	}
	?>
	
	<body onunload="window.opener.location.reload();">
		<form id="msform" action="traitement_csv.php" method="post" enctype="multipart/form-data" >
			<fieldset>
				<h2 class="fs-title"><?php echo $texte;?></h2>
				<input type="file" name="Parcourir" class="next action-buttons" value="Parcourir" id="Parcourir"/>
  				<input type="submit" name="Submit" value="Submit" /> 
			</fieldset>
		</form>
	</body>
</html>