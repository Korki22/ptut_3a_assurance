$(function(){
    //original field values
    var field_values = {
            //id        :  value
            'email'  : 'email address',
            'password'  : 'password',
            'cpassword' : 'password',
            'siret'  : 'siret',
            'rsociale'  : 'rsociale',
            'telephone'  : 'telephone',
            'rue' : 'rue',
            'ville' : 'ville',
            'codepostal' : 'codepostal'
    };


    //inputfocus
    $('input#email').inputfocus({ value: field_values['email'] });
    $('input#password').inputfocus({ value: field_values['password'] });
    $('input#cpassword').inputfocus({ value: field_values['cpassword'] }); 
    $('input#siret').inputfocus({ value: field_values['siret'] });
    $('input#rsociale').inputfocus({ value: field_values['rsociale'] });
    $('input#telephone').inputfocus({ value: field_values['telephone'] }); 
    $('input#rue').inputfocus({ value: field_values['rue'] });
    $('input#ville').inputfocus({ value: field_values['ville'] });
    $('input#codepostal').inputfocus({ value: field_values['codepostal'] });



    //reset progress bar
    $('#progress').css('width','0');
    $('#progress_text').html('0% Complete');

    //first_step
    $('form').submit(function(){ return false; });
    $('#submit_first').click(function(){
        //remove classes
        $('#first_step input').removeClass('error').removeClass('valid');

        //ckeck if inputs aren't empty
        var fields = $('#first_step input[type=text], #first_step input[type=password]');
        var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/; 
        var error = 0;
        fields.each(function(){
            var value = $(this).val();
            if( value.length<6 || value==field_values[$(this).attr('id')] || ( $(this).attr('id')=='email' && !emailPattern.test(value) ) ) {
                $(this).addClass('error');
                $(this).effect("shake", { times:3 }, 50);
                
                error++;
            } else {
                $(this).addClass('valid');
            }
        });

        
        if(!error) {
            if( $('#password').val() != $('#cpassword').val() ) {
                    $('#first_step input[type=password]').each(function(){
                        $(this).removeClass('valid').addClass('error');
                        $(this).effect("shake", { times:3 }, 50);
                    });
                    
                    return false;
            } else {   
                //update progress bar
                $('#progress_text').html('25% Complete');
                $('#progress').css('width','85px');
                
                //slide steps
                $('#first_step').slideUp();
                $('#second_step').slideDown();     
            }               
        } else return false;
    });



    $('#submit_second').click(function(){
        //remove classes
        $('#second_step input').removeClass('error').removeClass('vaild');
        var  telPattern = /[0-9]{10}/;
        var siretPattern = /[0-9]{14}/;
        var fields = $('#second_step input[type=text]');
        var error = 0;
        fields.each(function(){
            var value = $(this).val();
            if( value.length<1 || value==field_values[$(this).attr('id')] || ( $(this).attr('id')=='telephone' && !telPattern.test(value) ) || ( $(this).attr('id')=='siret' && !siretPattern.test(value) ) ) {
                $(this).addClass('error');
                $(this).effect("shake", { times:3 }, 50);
                
                error++;
            } else {
                $(this).addClass('valid');
            }
        });

        if(!error) {
                //update progress bar
                $('#progress_text').html('50% Complete');
                $('#progress').css('width','170px');
                
                //slide steps
                $('#second_step').slideUp();
                $('#third_step').slideDown();     
        } else return false;

    });


    $('#submit_third').click(function(){
        //remove classes
        $('#third_step input').removeClass('error').removeClass('vaild');
        var cpPattern = /[0-9]{5}/;
        var fields = $('#third_step input[type=text]');
        var error = 0;
        fields.each(function(){
            var value = $(this).val();
            if( value.length<1 || value==field_values[$(this).attr('id')] || ( $(this).attr('id')=='codepostal' && !cpPattern.test(value) ) ) {
                $(this).addClass('error');
                $(this).effect("shake", { times:3 }, 50);
                
                error++;
            } else {
                $(this).addClass('valid');
            }
        });

        if(!error) {
                //update progress bar
                $('#progress_text').html('75% Complete');
                $('#progress').css('width','255px');
                
                //slide steps
                $('#third_step').slideUp();
                $('#fourth_step').slideDown();     
        } else return false;

    });



    $('#submit_fourth').click(function(){
        //update progress bar
        $('#progress_text').html('100% Complete');
        $('#progress').css('width','340px');

        //prepare the fifth step
        var fields = new Array(
            $('#rsociale').val(),
            $('#password').val(),
            $('#email').val(),
            $('#siret').val(),
            $('#telephone').val(),
            $('#gender').val(),
            $('#country').val()                       
        );
        var tr = $('#fifth_step tr');
        tr.each(function(){
            //alert( fields[$(this).index()] )
            $(this).children('td:nth-child(2)').html(fields[$(this).index()]);
        });
                
        //slide steps
        $('#fourth_step').slideUp();
        $('#fifth_step').slideDown();            
    });


    $('#submit_fifth').click(function(){
        //send information to server
        //document.location.href="traitement_ent.php";
       /*$.ajax({
              type: "POST",
              url: "traitement_ent.php",
              data: { password: $('#password').val()}
              success: function(output) {
                      alert(output);
                  }
            })*/
              
        //alert('Data sent');
        //  $(?form?).unbind(?submit?).submit(); 
         $('form').unbind('submit').submit();

    });

});


