<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<?php
    require_once('base.php');
  $bdd = connect_db();
?>
 <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>

    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <meta name="description" content="Projet Assuramort"/>
    <meta name="keywords" content="Assuramort, Assurance"/>
    <meta name="author" content="Geeks"/>

    <title>Assuramort</title>

    <link rel="stylesheet" type="text/css" href="css/styleent.css" media="all" />	
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/jquery-ui.min.js"></script>
    <script type="text/javascript" src="jquery/jquery.inputfocus-0.9.min.js"></script>
    <script type="text/javascript" src="jquery/jquery.main.js"></script>
</head>
<body>
	
    <div id="barfixed">
        <a href="index.php?content=accueil"><img src="images/home.png" alt="Retour accueil" float:right/></a>
        <p>Assuramort</p>
    </div>
    <div id="wrap">
            <header>
                <div class="clear2"></div>    
                <nav class="box-shadow-services" style="texte-decoration: none !important;">
                    <div>
                        <ul class="menu-services">
                            <li class="home-page"><a href="index.php?content=accueil" ><span></span></a></li>
                            <li><a href="index.php?content=contrat" style='text-decoration: none;'>Contrat</a></li>
                            <li><a href="infoEnt.php" style='text-decoration: none;'>Inscription</a></lnrei>
                        </ul>
                        <div class="social-icons">
                            <span><a href="javascript:void(0);" onclick="javascript:window.open('test.php','popup','resizable=no,scrollbars=yes,location=no,width=500,height=500,top=200,left=750');">Connexion</a></span>
                        </div>
                        <div class="clear2"></div>
                    </div>
                </nav>
            </header>
        </div>

	<div id="container">
        <form action="traitement_ent.php" method="post">
	
            <!-- #first_step -->
            <div id="first_step">
                <h1>INSCRIPTION <span>NOM ENTREPRISE</span></h1>

                <div class="form">
                    <input type="text" name="email" id="email" value="email"/>
                    <label for="email">Insérez l'email qui servira d'identifiant lors de vos connexions</label>
                    
                    <input type="password" name="password" id="password" value="password" />
                    <label for="password">Au moins 6 caracteres. Mixer des chiffres et des lettres pour un mot de passe puissant</label>
                    
                    <input type="password" name="cpassword" id="cpassword" value="password" />
                    <label for="cpassword">Au moins 6 caracteres. Mixer des chiffres et des lettres pour un mot de passe puissant.</label>
                </div>      <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
                <input class="submit" type="submit" name="submit_second" id="submit_first" value="" />
            </div>      <!-- clearfix --><div class="clear"></div><!-- /clearfix -->


            <!-- #second_step -->
            <div id="second_step">
                <h1>INFORMATIONS <span>PERSONNELLES</span></h1>

                <div class="form">
                    <input type="text" name="siret" id="siret" value="siret"/>
                    <label for="siret">N° de Siret (14 chiffres ex:12345678912345)</label>
                    <input type="text" name="rsociale" id="rsociale" value="rsociale" />
                    <label for="rsociale">Raison sociale. (au moins 1 caractère) </label>
                    <input type="text" name="telephone" id="telephone" value="telephone" />
                    <label for="telephone">Telephone. (10 chiffres ex:0102030405)</label>                   
                </div>      <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
                <input class="submit" type="submit" name="submit_second" id="submit_second" value="" />
            </div>      <!-- clearfix --><div class="clear"></div><!-- /clearfix -->


            <!-- #thrid_step -->
            <div id="third_step">
                <h1>INFORMATIONS <span>PERSONNELLES</span></h1>

                <div class="form">
                    <input type="text" name="rue" id="rue" value="rue"/>
                    <label for="rue">N° de Rue</label>
                    <input type="text" name="ville" id="ville" value="ville" />
                    <label for="ville">Ville</label>
                    <input type="text" name="codepostal" id="codepostal" value="codepostal" />
                    <label for="codepostal">Code postal (5 chiffres ex:69800)</label>                   
                </div>      <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
                <input class="submit" type="submit" name="submit_third" id="submit_third" value="" />
            </div>      <!-- clearfix --><div class="clear"></div><!-- /clearfix -->



            <!-- #fourth_step -->
            <div id="fourth_step">
                <h1>VOTRE CHOIX DE<span>FORMULE</span></h1>

                <div class="form">
                    <select id="gender" name="gender">
                        <option>Cotisations</option>
                        <option>Prestations</option>
                    </select>
                    <label for="gender">Quelle formule?</label> <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
                    
                    <select id="country" name="country">
                        <option>Mois</option>
                        <option>Trimestre</option>
                        <option>Semestre</option>
                        <option>Annee</option>
                    </select>
                    <label for="country">Periodicite </label> <!-- clearfix --><div class="clear"></div><!-- /clearfix -->


                    <select id="agence" name="agence">
                    <?php
                    $reqagence = $bdd->prepare("SELECT id_Agence, Nom_Agence FROM Agence");
                    $reqagence->execute() or die($reqagence->errorInfo());
                    while ($ligneagence=$reqagence->fetch(PDO::FETCH_ASSOC)){
                  $lignenom=$ligneagence["Nom_Agence"];
                  echo "<option>$lignenom</option>";
                    }
                    ?>
                    </select>
                    <label for="country">Agence</label> <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
                    
                </div>      <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
                <input class="submit" type="submit" name="submit_fourth" id="submit_fourth" value="" />
                
            </div>      <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
            
            
            <!-- #fourth_step -->
            <div id="fifth_step">
                <h1>RECAPITULATIF DE VOS INFOS <span>!</span></h1>

                <div class="form">
                    <h2>Summary</h2>
                    
                    <table>
                        <tr><td>Nom</td><td></td></tr>
                        <tr><td>Password</td><td></td></tr>
                        <tr><td>Email</td><td></td></tr>
                        <tr><td>Siret</td><td></td></tr>
                        <tr><td>Telephone</td><td></td></tr>
                        <tr><td>Formule</td><td></td></tr>
                        <tr><td>Periodicite</td><td></td></tr>
                    </table>
                </div>      <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
                <input class="send submit" type="submit" name="submit_fifth" id="submit_fifth" value="" />            
            </div>
            
        </form>
	</div>
	<div id="progress_bar">
        <div id="progress"></div>
        <div id="progress_text">0% Complete</div>
	</div>
	
<!-- </body>
</html> -->