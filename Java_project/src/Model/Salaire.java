/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Date;

/**
 *
 * @author Erwan
 */
public class Salaire {

    private Date debut;
    private Date fin;
    private String poste;
    private float salaire;

    public Salaire(Date debut, Date fin, String poste, float salaire) {
        this.debut = debut;
        this.fin = fin;
        this.poste = poste;
        this.salaire = salaire;
    }



    public Date getDebut() {
        return debut;
    }

    public Date getFin() {
        return fin;
    }

    public void setDebut(Date debut) {
        this.debut = debut;
    }

    public void setFin(Date fin) {
        this.fin = fin;
    }
}
