/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Model;

import Database.DAOFactory;
import View.TablePanel;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
Cette classe représente une liste générique de Modeles, comme une liste d'employés ou de salariés
*/
public class ListModel<GenericModel> extends Observable{
    
       private static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ListModel.class);
        
        public int curPage;
        public int resultsCount;
        public static final int RESULTS_PER_PAGES = 100;
        ArrayList<GenericModel> listModel;
        private int columnCount = 0;
        private String header[] = {""};
        Class type;
        String searchType;
        
        public ListModel(Class type, String searchType) {

        
            listModel = new ArrayList<GenericModel>();
            // Stockage du type d'objet traité
            this.type = type;
            this.searchType = searchType;
            // Récupération du header et du nombre de colonnes du modèle, et stockage dans la classe
            getColumnCountFromModel();
            getHeaderFromModel();
        }
        
        public int getSize() {
            return listModel.size();
        }
        
        public synchronized void addItem(GenericModel val) {
            listModel.add(val);
        }
        
        public synchronized String getValueAt(int row, int column) {
            try {
                Method getValueAt =  type.getMethod("getValueAt",int.class);
                return (String) getValueAt.invoke(listModel.get(row), column);
            } catch (    NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
               logger.error(ex.getMessage(), ex);
                return "";
            }
        }
        
        public synchronized void clear() {
            listModel.clear();
        }
        
        public int getColumnCount() {
            return columnCount;
        }

        public String[] getHeader() {
            return header;
        }
       
        private void getColumnCountFromModel() {
               try {
                // Récupération de la méthode statique "getColumnCount", pour le type de classe courant
                Method getColumnCount =  type.getMethod("getColumnCount");
                // Appel de la fonction
                columnCount = (int)(getColumnCount.invoke(null));
            } catch (    NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                logger.error(ex.getMessage(), ex);
            }

        }    
        
        private void getHeaderFromModel() {
            try {
                 // Récupération de la méthode statique "getHeader", pour le type de classe courant
                Method getHeader =  type.getMethod("getHeader");
                // Appel de la fonction
               header = (String[])(getHeader.invoke(null));
            } catch (    NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
               logger.error(ex.getMessage(), ex);
            }
        }
        
        public GenericModel getItem(int row) {
            return listModel.get(row);
        }
        
        public void search(String research) {
            // Nouvelle recherche, on retourne en page 0
            try {
            switch(searchType) {
                case TablePanel.SEARCH_EMPLOYES:
                    String [] values = research.split(":");
                    if(values.length == 1)
                    {
                        if(values[0].matches("^[0-9]+$"))
                        {
                            resultsCount = DAOFactory.getSalarieDAO().getListSalarie((ListModel<Salarie>)this, RESULTS_PER_PAGES*curPage, RESULTS_PER_PAGES,null, Integer.parseInt(values[0]));
                        }
                        else
                            resultsCount = DAOFactory.getSalarieDAO().getListSalarie((ListModel<Salarie>)this, RESULTS_PER_PAGES*curPage, RESULTS_PER_PAGES,research);
                    }
                    else
                        resultsCount = DAOFactory.getSalarieDAO().getListSalarie((ListModel<Salarie>)this, RESULTS_PER_PAGES*curPage, RESULTS_PER_PAGES,values[1], Integer.parseInt(values[0]));
                    break;
                case TablePanel.SEARCH_ENTREPRISES:
                    resultsCount = DAOFactory.getEntrepriseDAO().SearchEntrepriseByName((ListModel<Entreprise>)this, research, RESULTS_PER_PAGES*curPage, RESULTS_PER_PAGES, false);
                    break;
                case TablePanel.HOME:
                    resultsCount = DAOFactory.getEntrepriseDAO().SearchEntrepriseByName((ListModel<Entreprise>)this, research, RESULTS_PER_PAGES*curPage, RESULTS_PER_PAGES, true);
                    break;
            }
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
            }
            this.setChanged();
            this.notifyObservers();
        }
        
        public boolean isFirstPage() {
            return (curPage == 0) ? true : false;
        }
        
        public boolean isLastPage() {
            if(resultsCount <= (curPage +1) * RESULTS_PER_PAGES)
                return true;
            else return false;
        }
        
       
}
