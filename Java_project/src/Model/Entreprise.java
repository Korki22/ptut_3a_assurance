/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Tools.Utility;
import java.sql.Date;

/**
 *
 * @author Erwan
 */
public class Entreprise implements GenericModel {

    private int id;
    private String raisonSociale;
    private Adresse ad;
    private String email;
    private int numTel;
    private String pass;
    private String numSiret;
    private int idPer;
    private int idForm;
    private int idStatut;
    private Date dateEffet;
    private Date dateAdh;
    private int idAgence;
    private int nbSalaries;
    private boolean actionNeeded;

    public Entreprise(int id, String raisonSociale, Adresse ad, String email, int numTel, String pass, String numSiret, int idPer, int idForm, int idStatut, Date dateEffet, Date dateAdh, int idAgence, int nbEmployes) {
        this.id = id;
        this.raisonSociale = raisonSociale;
        this.ad = ad;
        this.email = email;
        this.numTel = numTel;
        this.pass = pass;
        this.numSiret = numSiret;
        this.idPer = idPer;
        this.idForm = idForm;
        this.idStatut = idStatut;
        this.dateEffet = dateEffet;
        this.dateAdh = dateAdh;
        this.idAgence = idAgence;
        this.actionNeeded = Maps.flag(id);
        this.nbSalaries = nbEmployes;
    }

    public Adresse getAdresse() {
        return ad;
    }

    public Date getDateAdh() {
        return dateAdh;
    }

    public Date getDateEffet() {
        return dateEffet;
    }

    public String getEmail() {
        return email;
    }

    public int getIdAgence() {
        return idAgence;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRaisonSociale() {
        return raisonSociale;
    }

    public void setRaisonSociale(String raisonSociale) {
        this.raisonSociale = raisonSociale;
    }

    public int getNumTel() {
        return numTel;
    }

    public void setNumTel(int numTel) {
        this.numTel = numTel;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String password) {
        this.pass = Utility.getHash(password);
    }

    public String getNumSiret() {
        return numSiret;
    }

    public void setNumSiret(String numSiret) {
        this.numSiret = numSiret;
    }

    public int getIdPer() {
        return idPer;
    }

    public void setIdPer(int idPer) {
        this.idPer = idPer;
    }

    public int getIdForm() {
        return idForm;
    }

    public void setIdForm(int idForm) {
        this.idForm = idForm;
    }

    public int getIdStatut() {
        return idStatut;
    }

    public void setNbSalaries(int nbSalaries) {
        this.nbSalaries = nbSalaries;
    }

    public void setIdStatut(int idStatut) {
        this.idStatut = idStatut;
    }

    public void setIdAgence(int idAgence) {
        this.idAgence = idAgence;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setDateEffet(Date dateEffet) {
        this.dateEffet = dateEffet;
    }

    public void setDateAdh(Date dateAdh) {
        this.dateAdh = dateAdh;
    }

    public void setAdresse(Adresse ad) {
        this.ad = ad;
    }

    public String getIdFormAsString() {
        return Maps.getFormule(this.idForm);
    }

    public String getIdPerAsString() {
        return Maps.getPeriodicite(this.idPer);
    }

    public String getIdStatutAsString() {
        return Maps.getStatut(this.idStatut);
    }

    public int getNbSalaries() {
        return nbSalaries;
    }

    public void setActionNeeded(boolean actionNeeded) {
        this.actionNeeded = actionNeeded;
    }

    public boolean isActionNeeded() {
        return actionNeeded;
    }

    public String getValueAt(int index) {
        switch (index) {
            case 0:
                return Integer.toString(this.getId());
            case 1:
                return this.getRaisonSociale();
            case 2:
                return Integer.toString(this.getNbSalaries());
            case 3:
                return this.getEmail();
            case 4:
                return Integer.toString(this.getNumTel());
            case 5:
                return  Maps.getStatut(this.idStatut);  // Statut
            default:
                return "";
        }
    }

    // Toutes les classes Modèle qui doivent pouvoir s'afficher dans un tableau doivent implémenter ces deux méthodes :
    public static int getColumnCount() {
        return 6;
    }

    public static String[] getHeader() {
        String s[] = {"ID", "Raison sociale", "Nombre de salariés", "Mail", "Téléphone", "Statut"};
        return s;
    }
    
    @Override
    public String getTitle() {
        return "Entreprise " + this.raisonSociale;
    }
    
    @Override
    public Object[][] getData() {
        Object[][] data = {
        {"Raison sociale :", this.getRaisonSociale()},
        {"Numéro de siret :", this.getNumSiret()},
        {"Adresse :", this.getAdresse()},
        {"Email :", this.getEmail()},
        {"Numéro de téléphone :", this.getNumTel()},
        {"Agence de référence :", this.getIdAgence()},  
        {"Periodicité du versement :", this.getIdPer()},
        {"Formule du contrat :", this.getIdAgence()}, 
        {"Statut :", this.getIdStatut()}
        };
        return data;
    }

    @Override
    public void updateModel(Object[] data) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
