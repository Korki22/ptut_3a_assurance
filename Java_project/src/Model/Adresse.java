/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Erwan
 */
public class Adresse {
    String voie;
    String ville;
    String cp;

    public Adresse(String voie, String ville, String cp) {
        this.voie = voie;
        this.ville = ville;
        this.cp = cp;
    }

    public String getCp() {
        return cp;
    }

    public String getVille() {
        return ville;
    }

    public String getVoie() {
        return voie;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public void setVoie(String voie) {
        this.voie = voie;
    }

    @Override
    public String toString() {
        return this.voie + " " + String.valueOf(this.cp) + " " + this.ville;
    }    
}
