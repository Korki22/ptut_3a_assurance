/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Database.DAOFactory;
import java.sql.Date;
import java.sql.SQLException;
import Tools.Utility;
import javax.swing.JComboBox;
/**
 *
 * @author Erwan
 */
public class Salarie extends Personne implements GenericModel {

    private Adresse adresse;
    private String email;
    private int nbEnfants;
    private int numTel;
    private Personne conjoint;
    private int ageDiffere;
    private int idEntreprise;
    private String entreprise;
    private int idSitFam;
    private int idBaseCal;
    private int idModVers;
    private int idStatut;
    private boolean actionNeeded;
    private int idAgence;
    private int idFormule;
    private double cotisation; //montant de la cotisation que le salarié fera par an
    private InfoSalarie info;
    //password destiné à être récupéré déjà hashé depuis la table
    private String password;

    public Salarie(Adresse adresse, String email, int nbEnfants, int numTel, Personne conjoint, int ageDiffere, int idEntreprise, int idSitFam, int idBaseCal, int idModVers, int idStatut, String password, int id, String nom, String prenom, Date birthdate, String ent, int agence, int formule, double cotisation) {
        super(id, nom, prenom, birthdate);
        this.adresse = adresse;
        this.email = email;
        this.nbEnfants = nbEnfants;
        this.numTel = numTel;
        this.conjoint = conjoint;
        this.ageDiffere = ageDiffere;
        this.idEntreprise = idEntreprise;
        this.idSitFam = idSitFam;
        this.idBaseCal = idBaseCal;
        this.idModVers = idModVers;
        this.idStatut = idStatut;
        this.password = password;
        this.actionNeeded = Maps.flag(id);
        this.entreprise = ent;
        this.idAgence = agence;
        this.idFormule = formule;
        this.cotisation = cotisation;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public int getAgeDiffere() {
        return ageDiffere;
    }

    public Personne getConjoint() {
        return conjoint;
    }

    public String getEmail() {
        return email;
    }

    public double getCotisation() {
        return cotisation;
    }

    public void setCotisation(double cotisation) {
        this.cotisation = cotisation;
    }

    public int getNbEnfants() {
        return nbEnfants;
    }

    public int getNumTel() {
        return numTel;
    }

    public void setIdAgence(int idAgence) {
        this.idAgence = idAgence;
    }

    public void setIdSitFam(int idSitFam) {
        this.idSitFam = idSitFam;
    }

    public void setIdBaseCal(int idBaseCal) {
        this.idBaseCal = idBaseCal;
    }

    public void setIdEntreprise(int idEntreprise) {
        this.idEntreprise = idEntreprise;
    }

    public void setIdModVers(int idModVers) {
        this.idModVers = idModVers;
    }

    public void setIdStatut(int idStatut) {
        this.idStatut = idStatut;
    }
    
    public void setAdresse(Adresse ad) {
        this.adresse = ad;
    }

    public void setAgeDiffere(int ageDiffere) {
        this.ageDiffere = ageDiffere;
    }

    public void setConjoint(Personne conjoint) {
        this.conjoint = conjoint;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setNbEnfants(int nbEnfants) {
        this.nbEnfants = nbEnfants;
    }

    public void setNumTel(int numTel) {
        this.numTel = numTel;
    }

    public void setPassword(String password) {
        this.password = Utility.getHash(password);
    }

    public String getPassword() {
        return password;
    }

    public int getIdEntreprise() {
        return idEntreprise;
    }

    public int getIdBaseCal() {
        return idBaseCal;
    }

    public int getIdModVers() {
        return idModVers;
    }

    public int getIdSitFam() {
        return idSitFam;
    }

    public int getIdStatut() {
        return idStatut;
    }

    public void setActionNeeded(boolean actionNeeded) {
        this.actionNeeded = actionNeeded;
    }

    public boolean isActionNeeded() {
        return actionNeeded;
    }

    public String getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(String entreprise) {
        this.entreprise = entreprise;
    }

    public InfoSalarie getInfo() throws SQLException {
        if (info == null) {
            DAOFactory.getSalarieDAO().loadInformations(this);
        }
        return info;
    }

    public void setInfo(InfoSalarie info) {
        this.info = info;
    }

    public String getIdBaseCalAsString() {
        return Maps.getOptionPrestation(this.idBaseCal);
    }

    public String getIdModVersAsString() {
        return Maps.getModeVersement(this.idModVers);
    }

    public String getIdSitFamAsString() {
        return Maps.getSituationFamialie(this.idSitFam);
    }

    public String getIdStatutAsString() {
        return Maps.getStatut(this.idStatut);
    }
    
    public void freeInfo()
    {
        this.info = null;
    }

    public String getValueAt(int index) {
        switch (index) {
            case 0:
                return Integer.toString(this.getId());
            case 1:
                return this.getNom();
            case 2:
                return this.getPrenom();
            case 3:
                return this.entreprise;  // Entreprise
            case 4:
                return this.getEmail();
            case 5:
                return Integer.toString(this.getNumTel());
            case 6:
                return Maps.getStatut(idStatut);  // Statut
            default:
                return "";
        }
    }

    // Toutes les classes Modèle qui doivent pouvoir s'afficher dans un tableau doivent implémenter ces deux méthodes :
    public static int getColumnCount() {
        return 7;
    }

    public static String[] getHeader() {
        String s[] = {"ID", "Nom", "Prénom", "Entreprise", "Mail", "Téléphone", "Statut"};
        return s;
    }

    @Override
    public String getTitle() {
        return "Salarié " + this.getNom() + " " + this.getPrenom();
    }

    @Override
    public Object[][] getData() {
        Object[][] data = {
            {"Nom :", this.getNom()},
            {"Prenom :", this.getPrenom()},
            {"Entreprise :", this.getIdEntreprise()},
            {"Addresse :", this.getAdresse()},
            {"Email :", this.getEmail()},
            {"Numéro de téléphone :", this.getNumTel()},
            {"Situation familiale :", new JComboBox<String>(Maps.situationFamilialeValues())},
            {"Agence de référence :", 0},
            {"Formule du contrat :", 0},
            {"Statut :", this.getIdStatut()}
        };
        return data;
    }

    @Override
    public void updateModel(Object[] data) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the idAgence
     */
    public int getIdAgence() {
        return idAgence;
    }

    /**
     * @return the idFormule
     */
    public int getIdFormule() {
        return idFormule;
    }

    /**
     * @param idFormule the idFormule to set
     */
    public void setIdFormule(int idFormule) {
        this.idFormule = idFormule;
    }

}
