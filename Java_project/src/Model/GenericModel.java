/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Win_V2
 */
public abstract interface GenericModel {
    public abstract String getTitle();
    public abstract Object[][] getData();
    public abstract void updateModel(Object[] data);
    
}
