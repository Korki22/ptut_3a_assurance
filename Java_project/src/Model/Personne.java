/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Date;

/**
 *
 * @author Erwan
 */
public class Personne {
    private int id;
    private String nom;
    private String prenom;
    private Date birthdate;

    public Personne(int id, String nom, String prenom, Date birthdate) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.birthdate = birthdate;
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }
    
    @Override
    public String toString() {
        return String.valueOf(this.id) + " " + this.nom + " " + this.prenom;
    }   
    
    
}
