/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 *
 * @author Erwan
 */
public class InfoSalarie {
    
    private final int idSalarie;    
    private HashMap<Date, Float> prime;
    private HashMap<Date, Float> cotisation;
    private ArrayList<Beneficiaire> benefeciaire;
    private ArrayList<Salaire> salaire;

    public InfoSalarie(int idSalarie) {
        this.idSalarie = idSalarie;
    }

    public ArrayList<Salaire> getSalaire() {
        return salaire;
    }

    public void setSalaire(ArrayList<Salaire> salaire) {
        this.salaire = salaire;
    }

    public ArrayList<Beneficiaire> getBenefeciaire() {
        return benefeciaire;
    }

    public HashMap<Date, Float> getCotisation() {
        return cotisation;
    }

    public HashMap<Date, Float> getPrime() {
        return prime;
    }

    public void setBenefeciaire(ArrayList<Beneficiaire> benefeciaire) {
        this.benefeciaire = benefeciaire;
    }

    public void setCotisation(HashMap<Date, Float> cotisation) {
        this.cotisation = cotisation;
    }

    public void setPrime(HashMap<Date, Float> prime) {
        this.prime = prime;
    }

    public int getIdSalarie() {
        return idSalarie;
    }   
    
    public float getCotisationPerYear(int year)
    {
        float total = 0;
        Calendar cal = Calendar.getInstance();
        for(Date d : prime.keySet())
        {
            cal.setTime(d);
            if(year == cal.get(Calendar.YEAR))
            {
                total += prime.get(d);
            }
        }       
        for(Date d : cotisation.keySet())
        {
            cal.setTime(d);
            if(year == cal.get(Calendar.YEAR))
            {
                total += cotisation.get(d);
            }
        }
        return total;
    }
}
