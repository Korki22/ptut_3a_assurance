/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Tools.Utility;
import java.sql.Date;

/**
 *
 * @author Erwan
 */
public class Agent extends Personne{
    //password to hash later
    private String password;
    private String login;
    private int idAgence;

    public Agent(String password, String login, int idAgence, int id, String nom, String prenom, Date birthdate) {
        super(id, nom, prenom, birthdate);
        this.password = password;
        this.login = login;
        this.idAgence = idAgence;
    }

    public int getIdAgence() {
        return idAgence;
    }

    public String getPassword() {
        return password;
    }

    public String getLogin() {
        return login;
    }

    public void setIdAgence(int idAgence) {
        this.idAgence = idAgence;
    }

    public void setLogin(String login) {
        this.login = login;
    }

        public void setPassword(String password) {
        this.password = Utility.getHash(password);
    }
}
