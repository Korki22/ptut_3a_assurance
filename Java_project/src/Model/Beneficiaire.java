/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Database.DAOFactory;
import java.sql.Date;
import java.sql.SQLException;

/**
 *
 * @author Erwan
 */
public class Beneficiaire extends Personne{
    private String lienParente;
    private float pourcentage;
    private int idSalarie;

    public Beneficiaire(String lienParente, float pourcentage, int IDSalarie, int id, String nom, String prenom, Date birthdate) {
        super(id, nom, prenom, birthdate);
        this.lienParente = lienParente;
        this.pourcentage = pourcentage;
        this.idSalarie = IDSalarie;
    }

    public int getIdSalarie() {
        return idSalarie;
    }

    public String getLienParente() {
        return lienParente;
    }

    public float getPourcentage() {
        return pourcentage;
    }

    public void setIdSalarie(int idSalarie) {
        this.idSalarie = idSalarie;
    }

    public void setLienParente(String lienParente) {
        this.lienParente = lienParente;
    }

    public boolean setPourcentage(float pourcentage) {
        if(pourcentage <= 0 || pourcentage > 1)
            return false;
        this.pourcentage = pourcentage;
        return true;
    }
    
    public Salarie getSalarie() throws SQLException
    {
        return DAOFactory.getSalarieDAO().find(this.idSalarie);
    }
}
