/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Erwan
 */
public class Maps {

    static private HashMap<Integer, String> modeVersement;
    static private HashMap<Integer, String> situationFamiliale;
    static private HashMap<Integer, String> optionPrestation;
    static private HashMap<Integer, String> statut;
    static private HashMap<Integer, String> formule;
    static private HashMap<Integer, String> periodicite;
    static private HashMap<Integer, String> agence;
    static private ArrayList<Integer> statutActionNeeded;

    static public void addSatut(int id, String label, boolean flag) {
        statut.put(id, label);
        if (flag) {
            statutActionNeeded.add(id);
        }
    }

    static public void addModeVersement(int id, String label) {
        modeVersement.put(id, label);
    }

    static public void addSituationFamiliale(int id, String label) {
        situationFamiliale.put(id, label);
    }

    static public void addOptionPrestation(int id, String label) {
        optionPrestation.put(id, label);
    }

    static public void addFormule(int id, String label) {
        formule.put(id, label);
    }

    static public void addPeriodicite(int id, String label) {
        periodicite.put(id, label);
    }

    static public void addAgence(int id, String label) {
        agence.put(id, label);
    }

    /*static public void addPoste(int id, String label) {
     poste.put(id, label);
     }*/
    static public String getStatut(int id) {
        return statut.get(id);
    }

    /*static public String getPoste(int id) {
     return poste.get(id);
     }*/
    static public String getModeVersement(int id) {
        return modeVersement.get(id);
    }

    static public String getOptionPrestation(int id) {
        return optionPrestation.get(id);
    }

    static public String getSituationFamialie(int id) {
        return situationFamiliale.get(id);
    }

    static public String getFormule(int id) {
        return formule.get(id);
    }

    static public String getPeriodicite(int id) {
        return periodicite.get(id);
    }

    static public String getAgence(int id) {
        return agence.get(id);
    }

    static public boolean flag(int id) {
        return statutActionNeeded.contains(id);
    }

    public static void instanciate() throws SQLException {
        modeVersement = new HashMap<>();
        situationFamiliale = new HashMap<>();
        optionPrestation = new HashMap<>();
        statut = new HashMap<>();
        formule = new HashMap<>();
        periodicite = new HashMap<>();
        statutActionNeeded = new ArrayList<>();
        agence = new HashMap<>();
    }
    public static String[] situationFamilialeValues() {
        String[] tab = new String[situationFamiliale.size()];
        situationFamiliale.values().toArray(tab);
        return tab;
    }

    public static String[] optionPrestationValues() {
        String[] tab = new String[optionPrestation.size()];
        optionPrestation.values().toArray(tab);
        return tab;
    }

    public static String[] modeVersementValues() {
        String[] tab = new String[modeVersement.size()];
        modeVersement.values().toArray(tab);
        return tab;
    }

    public static String[] statutValues() {
        String[] tab = new String[statut.size()];
        statut.values().toArray(tab);
        return tab;
    }

    public static String[] formuleValues() {
        String[] tab = new String[formule.size()];
        formule.values().toArray(tab);
        return tab;
    }

    public static String[] periodiciteValues() {
        String[] tab = new String[periodicite.size()];
        periodicite.values().toArray(tab);
        return tab;
    }
    
    public static String[] agenceValues() {
        String[] tab = new String[agence.size()];
        agence.values().toArray(tab);
        return tab;
    }
}
