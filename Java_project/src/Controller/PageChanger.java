/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.ListModel;
import View.TablePanel;

/**
 *
 * @author Win_V2
 */
public class PageChanger implements Runnable {

    public enum ChangePageType {

        PAGE_NEXT, PAGE_PREV, PAGE_BEGIN, PAGE_END
    }
    ChangePageType type;
    TablePanel view;
    ListModel<?> model;
    String researchString = "";

    public PageChanger(TablePanel view) {
        this.view = view;
    }
    
    public void setResearchString( String researchString) {
        this.researchString = researchString; 
    }

    public void init(ChangePageType type) {
        model = view.getDataList();
        this.type = type;
    }

    @Override
    public void run() {
        changePage(type);
    }

    public void changePage(ChangePageType type) {
        switch (type) {
            case PAGE_BEGIN:     
                    model.curPage = 0;
                    model.search(researchString);
                break;
            case PAGE_PREV:
                if (!model.isFirstPage()) {
                    model.curPage--;
                    model.search(researchString);
                }
                break;
            case PAGE_NEXT:
                if (!model.isLastPage()) {
                    model.curPage++;
                    model.search(researchString);
                }
                break;
            case PAGE_END:
                if (!model.isLastPage()) {
                    model.curPage = ((int) Math.ceil(model.resultsCount / model.RESULTS_PER_PAGES)) -1;
                    if( model.curPage < 0) {
                        model.curPage = 0;
                    }
                    model.search(researchString);
                }
                break;
        }
    }
}
