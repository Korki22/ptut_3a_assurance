/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Database.SalarieDAO;
import Model.Adresse;
import Model.GenericModel;
import Model.Maps;
import Model.Salarie;
import java.util.ArrayList;

/**
 *
 * @author Win_V2
 */
public class ModelController {

    
    
    public String control(GenericModel model, ArrayList<String> values) {
        if (model instanceof Salarie) {
            Salarie s = ((Salarie) model);
            
            for(String str : values)  {
                if(str.equals("")) return "Tout les champs ne sont pas renseignés";
            }
            
            try {
                //Numéro de téléphone :
                Integer.parseInt(values.get(7));
            } catch (Exception e) {
                return "Erreur dans le numéro de téléphone";
            }
            
            try {
            Double.parseDouble(values.get(12));
            } catch (Exception e) {
                return "Erreur dans le montant des cotisations";
            }
            
            //Nom
             s.setNom(values.get(0));
            //Prenom :
             s.setPrenom(values.get(1));
            
            //Entreprise :
            
            
            //Rue :
             //Code postal :
             //Ville :
            s.setAdresse(new Adresse(values.get(3),values.get(5),values.get(4)));

            //Email :
            s.setEmail(values.get(6));
            
            //Numéro de téléphone :
            s.setNumTel(Integer.parseInt(values.get(7)));
            
            //Situation familiale :
            s.setIdSitFam(Integer.parseInt(values.get(8))+1);
            
            // Cotisations 

                //Numéro de téléphone :
             s.setCotisation(Double.parseDouble(values.get(12)));
                        
            try {
                SalarieDAO.getSalarieDAO().update(s);
            } catch (Exception e) {
                 return "Erreur lors de l'insertion dans la base de données";
            }
        }

        return "";
    }
}
