/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import Model.Agent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Epulapp
 */
public class AgentDAO extends DAO<Agent> {

    static private DAO instance;

    private AgentDAO() {
    }

    public static AgentDAO getAgentDAO() {
        if (instance == null) {
            instance = new AgentDAO();
        }
        return (AgentDAO) instance;
    }

    @Override
    public Agent find(int id) throws SQLException {
        String query = "Select * from Agent where ID_agent = " + String.valueOf(id);
        Statement stmt;
        stmt = ConnectMySQL.getConnection().createStatement();
        ResultSet rs = stmt.executeQuery(query);
        Agent a = null;
        while (rs.next()) {
            a = new Agent(rs.getString("Pass_agent"), rs.getString("Login_agent"), rs.getInt("ID_agence"), rs.getInt("ID_agent"), rs.getString("Nom_agent"), rs.getString("Prenom_agent"), rs.getDate("Date_naissance_agent"));
        }
        rs.close();
        stmt.close();
        ConnectMySQL.closeConnection();
        return a;
    }
    
    public int connect(String login, String password, int returnAgentID) throws Exception {
        String query = "Select ID_Agent from Agent where Login_agent = '" + login + "' and Pass_agent = '" + password + "'";
        Statement stmt;
        stmt = ConnectMySQL.getConnection().createStatement();
        ResultSet rs = stmt.executeQuery(query);
        if(rs.next()) {
            return rs.getInt("ID_Agent");
        }
        rs.close();
        stmt.close();
        ConnectMySQL.closeConnection();
        return -1;
    }

    @Override
    public void create(Agent obj) throws SQLException {

        String query = "INSERT INTO Agent ('ID_agent', 'Prenom_agent', 'Date_naissance_agent', 'Nom_Agent', 'Pass_agent', 'Login_agent', 'ID_agence')"
                + "VALUES (NULL,?,?,?,?,?,?)";

        PreparedStatement stmt = ConnectMySQL.getConnection().prepareStatement(query);
        stmt.setString(1, obj.getPrenom());
        stmt.setDate(2, obj.getBirthdate());
        stmt.setString(3, obj.getNom());
        stmt.setString(4, obj.getPassword());
        stmt.setString(5, obj.getLogin());
        stmt.setInt(6, obj.getIdAgence());
        stmt.executeUpdate();
        stmt.close();
        ConnectMySQL.closeConnection();
    }

    @Override
    public void update(Agent obj) throws SQLException {
        Statement stmt;
        ResultSet rs;
        String query = "Select * from Agent where ID_agent = " + String.valueOf(obj.getId());
        stmt = ConnectMySQL.getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
        rs = stmt.executeQuery(query);
        if (rs.first()) {
            rs.updateString("Nom_agent", obj.getNom());
            rs.updateString("Prenom_agent", obj.getPrenom());
            rs.updateDate("Date_naissance_agent", obj.getBirthdate());
            rs.updateString("Pass_agent", obj.getPassword());
            rs.updateInt("ID_agence", obj.getIdAgence());
            rs.updateString("Login_agent", obj.getLogin());
            rs.updateRow();
            rs.close();
            stmt.close();
            ConnectMySQL.closeConnection();
        }
    }

    @Override
    public void delete(Agent obj) throws SQLException {
        String query = "Delete * from Agent where ID_agent = " + String.valueOf(obj.getId());
        Statement stmt;
        stmt = ConnectMySQL.getConnection().createStatement();
        stmt.executeUpdate(query);
        stmt.close();
        ConnectMySQL.closeConnection();
    }
}
