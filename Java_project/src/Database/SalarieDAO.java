/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import Model.Adresse;
import Model.InfoSalarie;
import Model.ListModel;
import Model.Personne;
import Model.Salarie;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Calendar;
import java.util.HashMap;

/**
 *
 * @author Erwan
 */
public class SalarieDAO extends DAO<Salarie> {

    static private DAO instance;

    public static SalarieDAO getSalarieDAO() throws SQLException {
        if (instance == null) {
            instance = new SalarieDAO();
        }
        return (SalarieDAO) instance;
    }

    @Override
    public Salarie find(int id) throws SQLException {
        String query = "Select * from Salarie_view where ID_salarie = " + String.valueOf(id);
        Statement stmt;
        stmt = ConnectMySQL.getConnection().createStatement();
        ResultSet rs = stmt.executeQuery(query);
        Salarie s = null;
        while (rs.next()) {
            Adresse ad = new Adresse(rs.getString("Nom_rue_salarie"), rs.getString("Ville_salarie"), rs.getString("CP_salarie"));
            Personne conjoint = new Personne(-1, rs.getString("Nom_conjoint_salarie"), rs.getString("Prenom_conjoint_salarie"), rs.getDate("Date_naissance_conjoint_salarie"));
            s = new Salarie(ad, rs.getString("Email_salarie"), rs.getInt("Nombre_enfants_salarie"), rs.getInt("Num_tel_salarie"), conjoint, rs.getInt("Age_debut_differe"),
                    rs.getInt("ID_entreprise"), rs.getInt("ID_situation_familiale"), rs.getInt("ID_base_calcul"), rs.getInt("ID_mode_versement"), rs.getInt("ID_statut"), rs.getString("Pass_salarie"),
                    rs.getInt("ID_salarie"), rs.getString("Nom_salarie"), rs.getString("Prenom_salarie"), rs.getDate("Date_naissance_salarie"), rs.getString("Raison_sociale"), rs.getInt("ID_agence"), rs.getInt("ID_Formule"), rs.getDouble("Cotisation"));
        }
        rs.close();
        stmt.close();
        ConnectMySQL.closeConnection();
        return s;
    }

    @Override
    public void create(Salarie obj) throws SQLException {
        String query = "INSERT INTO Salarie ('ID_salarie', 'Nom_salarie', 'Prenom_salarie', 'Date_naissance_salarie', 'Ville_salarie', 'CP_salarie', 'Nom_rue_salarie',"
                + " 'Nombre_enfants_salarie', 'Nom_conjoint_salarie', 'Prenom_conjoint_salarie', 'Date_naissance_conjoint_salarie', 'Age_debut_differe', 'Email_salarie',"
                + " 'Pass_salarie', 'Num_tel_salarie', 'ID_entreprise', 'ID_situation_familiale', 'ID_base_calcul', 'ID_mode_versement', 'ID_statut', 'Cotisation') VALUES (NULL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        PreparedStatement stmt = ConnectMySQL.getConnection().prepareStatement(query);
        stmt.setString(1, obj.getNom());
        stmt.setString(2, obj.getPrenom());
        stmt.setDate(3, obj.getBirthdate());
        stmt.setString(4, obj.getAdresse().getVille());
        stmt.setString(5, obj.getAdresse().getCp());
        stmt.setString(6, obj.getAdresse().getVoie());
        stmt.setInt(7, obj.getNbEnfants());
        stmt.setString(8, obj.getConjoint().getNom());
        stmt.setString(9, obj.getConjoint().getPrenom());
        stmt.setDate(10, obj.getConjoint().getBirthdate());
        stmt.setInt(11, obj.getAgeDiffere());
        stmt.setString(12, obj.getEmail());
        stmt.setString(13, obj.getPassword());
        stmt.setInt(14, obj.getNumTel());
        stmt.setInt(15, obj.getIdEntreprise());
        stmt.setInt(16, obj.getIdSitFam());
        stmt.setInt(17, obj.getIdBaseCal());
        stmt.setInt(18, obj.getIdModVers());
        stmt.setInt(19, obj.getIdStatut());
        stmt.setDouble(20, obj.getCotisation());
        stmt.executeUpdate();
        stmt.close();
        ConnectMySQL.closeConnection();
    }

    @Override
    public void update(Salarie obj) throws SQLException {
        Statement stmt = ConnectMySQL.getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet rs;
        String query = "Select * from Salarie where ID_salarie = " + String.valueOf(obj.getId());
        rs = stmt.executeQuery(query);
        if (rs.first()) {
            rs.updateString("Nom_salarie", obj.getNom());
            rs.updateString("Prenom_salarie", obj.getPrenom());
            rs.updateDate("Date_naissance_salarie", obj.getBirthdate());
            rs.updateString("Ville_salarie", obj.getAdresse().getVille());
            rs.updateString("CP_salarie", obj.getAdresse().getCp());
            rs.updateString("Nom_rue_salarie", obj.getAdresse().getVoie());
            rs.updateInt("Nombre_enfants_salarie", obj.getNbEnfants());
            rs.updateString("Nom_conjoint_salarie", obj.getConjoint().getNom());
            rs.updateString("Prenom_conjoint_salarie", obj.getConjoint().getPrenom());
            rs.updateDate("Date_naissance_conjoint_salarie", obj.getConjoint().getBirthdate());
            rs.updateInt("Age_debut_differe", obj.getAgeDiffere());
            rs.updateString("Email_salarie", obj.getEmail());
            rs.updateString("Pass_salarie", obj.getPassword());
            rs.updateInt("Num_tel_salarie", obj.getNumTel());
            rs.updateInt("ID_entreprise", obj.getIdEntreprise());
            rs.updateInt("ID_situation_familiale", obj.getIdSitFam());
            rs.updateInt("ID_base_calcul", obj.getIdBaseCal());
            rs.updateInt("ID_mode_versement", obj.getIdModVers());
            rs.updateInt("ID_statut", obj.getIdStatut());
            rs.updateDouble("Cotisation", obj.getCotisation());
            rs.updateRow();
            rs.close();
            stmt.close();
            ConnectMySQL.closeConnection();
        }
    }

    @Override
    public void delete(Salarie obj) throws SQLException {
        String query = "Delete * from Salarie where ID_salarie = " + String.valueOf(obj.getId());
        Statement stmt = ConnectMySQL.getConnection().createStatement();
        stmt.executeUpdate(query);
        stmt.close();
        ConnectMySQL.closeConnection();
    }

    /**
     * @param list
     * @param rowNumber, si rowNumber inférieur à 0 alors renvoie toutes les
     * lignes
     * @param count si count inférieur ou égal à 0 alors renvoie toutes les
     * lignes @param whereCondition contains the where condition of the query,
     * where keyword excluded, if no condition then whereClause = null
     * @return number of row found
     */
    public int getListSalarie(ListModel<Salarie> list, int rowNumber, int count) throws SQLException {
        return getListSalarie(list, rowNumber, count, null, -1);
    }

    /**
     * @param list
     * @param rowNumber, si rowNumber inférieur à 0 alors renvoie toutes les
     * lignes
     * @param count si count inférieur ou égal à 0 alors renvoie toutes les
     * lignes @param whereCondition contains the where condition of the query,
     * where keyword excluded, if no condition then whereClause = null
     * @param idEnt id de lentreprise à laquelle l'employé appartient
     * @return number of row found
     */
    public int getListSalarie(ListModel<Salarie> list, int rowNumber, int count, int idEnt) throws SQLException {
        return getListSalarie(list, rowNumber, count, null, idEnt);
    }

    /**
     * @param list
     * @param rowNumber, si rowNumber inférieur à 0 alors renvoie toutes les
     * lignes
     * @param count si count inférieur ou égal à 0 alors renvoie toutes les
     * lignes @param whereCondition contains the where condition of the query,
     * where keyword excluded, if no condition then whereClause = null
     * @param name nom de l'employé à rechercher
     * @return number of row found
     */
    public int getListSalarie(ListModel<Salarie> list, int rowNumber, int count, String name) throws SQLException {
        return getListSalarie(list, rowNumber, count, name, -1);
    }

    /**
     * @param list
     * @param rowNumber, si rowNumber inférieur à 0 alors renvoie toutes les
     * lignes
     * @param count si count inférieur ou égal à 0 alors renvoie toutes les
     * lignes @param whereCondition contains the where condition of the query,
     * where keyword excluded, if no condition then whereClause = null
     * @param name nom de l'employé à rechercher
     * @param idEnt id de lentreprise à laquelle l'employé appartient
     * @return number of row found
     */
    public int getListSalarie(ListModel<Salarie> list, int rowNumber, int count, String name, int idEnt) throws SQLException {
        String query = "Select * from Salarie_view";
        String args = "";
        boolean byName = false, byEnt = false;

        if (name != null && !name.equals("")) {
            byName = true;
            name = name.replace(" ", "");
        }
        if (idEnt != -1) {
            byEnt = true;
        }
        if (byEnt) {
            args += " where ID_entreprise = " + String.valueOf(idEnt);
        }
        if (byName) {
            if (byEnt) {
                args += " and ";
            } else {
                args += " where ";
            }
            args += "concat(Nom_salarie, Prenom_salarie) like'%" + name + "%' or concat(Prenom_salarie, Nom_salarie) like'%" + name + "%'";
        }

        if (rowNumber >= 0 && count > 0) {
            query += " " + args + " LIMIT " + String.valueOf(rowNumber) + "," + String.valueOf(count);
        }
        Statement stmt;
        stmt = ConnectMySQL.getConnection().createStatement();
        System.out.println(query);
        ResultSet rs = stmt.executeQuery(query);

        int nbLignes = -1;
        list.clear();
        while (rs.next()) {
            Adresse ad = new Adresse(rs.getString("Nom_rue_salarie"), rs.getString("Ville_salarie"), rs.getString("CP_salarie"));
            Personne conjoint = new Personne(-1, rs.getString("Nom_conjoint_salarie"), rs.getString("Prenom_conjoint_salarie"), rs.getDate("Date_naissance_conjoint_salarie"));
            list.addItem(new Salarie(ad, rs.getString("Email_salarie"), rs.getInt("Nombre_enfants_salarie"), rs.getInt("Num_tel_salarie"), conjoint,
                    rs.getInt("Age_debut_differe"), rs.getInt("ID_entreprise"), rs.getInt("ID_situation_familiale"), rs.getInt("ID_base_calcul"),
                    rs.getInt("ID_mode_versement"), rs.getInt("ID_statut"), rs.getString("Pass_salarie"), rs.getInt("ID_salarie"), rs.getString("Nom_salarie"),
                    rs.getString("Prenom_salarie"), rs.getDate("Date_naissance_salarie"), rs.getString("Raison_sociale"), rs.getInt("ID_agence"), rs.getInt("ID_formule"), rs.getDouble("Cotisation")));
        }

        query = "Select count(*) from Salarie_view";
        query += args;
        rs = stmt.executeQuery(query);
        if (rs.first()) {
            nbLignes = rs.getInt(1);
        }
        rs.close();
        stmt.close();
        ConnectMySQL.closeConnection();
        return nbLignes;
    }

    public void loadInformations(Salarie s) throws SQLException {
        int id = s.getId();

        InfoSalarie info = new InfoSalarie(id);

        //load beneficiaire
        info.setBenefeciaire(DAOFactory.getBeneficiaireDAO().getListBeneficiaire(id));

        //load cotisations
        info.setCotisation(getCotisationSalarie(id));

        //load primes
        info.setPrime(getPrimeSalarie(id));

        s.setInfo(info);
    }

    public HashMap<Date, Float> getCotisationSalarie(Salarie s) throws SQLException {
        return getCotisationSalarie(s.getId());
    }

    public HashMap<Date, Float> getCotisationSalarie(int id) throws SQLException {
        String query = "Select * from Cotisation where ID_salarie = " + String.valueOf(id);
        HashMap<Date, Float> primes = new HashMap<>();
        Statement stmt = ConnectMySQL.getConnection().createStatement();
        ResultSet rs = stmt.executeQuery(query);

        while (rs.next()) {
            primes.put(rs.getDate("Date_cotisation"), rs.getFloat("Montant_cotisation"));
        }

        rs.close();
        stmt.close();
        ConnectMySQL.closeConnection();
        return primes;
    }

    public HashMap<Date, Float> getPrimeSalarie(Salarie s) throws SQLException {
        return getPrimeSalarie(s.getId());
    }

    public HashMap<Date, Float> getPrimeSalarie(int id) throws SQLException {
        String query = "Select * from Prime where ID_salarie = " + String.valueOf(id);
        HashMap<Date, Float> primes = new HashMap<>();
        Statement stmt = ConnectMySQL.getConnection().createStatement();
        ResultSet rs = stmt.executeQuery(query);

        while (rs.next()) {
            primes.put(rs.getDate("Date_prime"), rs.getFloat("Montant_prime"));
        }

        rs.close();
        stmt.close();
        ConnectMySQL.closeConnection();
        return primes;
    }

    public float getCapitalPerYear(Salarie ent, int year) throws SQLException {
        return getCapitalPerYear(ent.getId(), year);
    }

    public float getCapitalPerYear(int idSal, int year) throws SQLException {
        String query = "{? = call getCapitalSalarie(?, ?)}";
        CallableStatement cs = ConnectMySQL.getConnection().prepareCall(query);
        cs.registerOutParameter(1, Types.FLOAT);
        cs.setInt(2, idSal);
        cs.setInt(3, year);
        cs.execute();
        float result = cs.getFloat(1);
        cs.close();
        ConnectMySQL.closeConnection();
        return result;
    }

    public void addPrime(Salarie s, Date date, Float montant) throws SQLException {
        addPrime(s.getId(), date, montant);
    }

    public void addPrime(int idSal, Date date, Float montant) throws SQLException {
        String query = "INSERT INTO `Prime`(`Date_Prime`, `Montant_prime`, `ID_salarie`) VALUES (?,?,?)";
        System.out.println(query);
        PreparedStatement stmt = ConnectMySQL.getConnection().prepareStatement(query);
        stmt.setDate(1, date);
        stmt.setFloat(2, montant);
        stmt.setInt(3, idSal);
        stmt.executeUpdate();
        stmt.close();
        ConnectMySQL.closeConnection();
    }

    public void addCotisation(Salarie s, Date date, Float montant) throws SQLException {
        addCotisation(s.getId(), date, montant);
    }

    public void addCotisation(int idSal, Date date, Float montant) throws SQLException {
        String query = "INSERT INTO Cotisation (`Date_cotisation`, `Montant_cotisation`, `ID_salarie`) VALUES (?,?,?)";
        PreparedStatement stmt = ConnectMySQL.getConnection().prepareStatement(query);
        stmt.setDate(1, date);
        stmt.setFloat(2, montant);
        stmt.setInt(3, idSal);
        stmt.executeUpdate();
        stmt.close();
        ConnectMySQL.closeConnection();
    }

    public float estimationViagere(Salarie s) throws SQLException {

        float estimation = 0;
        Calendar firstWage = Calendar.getInstance();
        double taux = s.getCotisation() / 1000;
        String query = "Select min(`Date_debut_salaire`) as 'Date' from Salaire where ID_salarie = " + String.valueOf(s.getId());
        Statement stmt;
        stmt = ConnectMySQL.getConnection().createStatement();
        ResultSet rs = stmt.executeQuery(query);
        if (rs.next()) {
            firstWage.setTime(rs.getDate("Date"));
        }
        Calendar birthDate = Calendar.getInstance();
        birthDate.setTime(s.getBirthdate());
        int age;
        age = firstWage.get(Calendar.YEAR) - birthDate.get(Calendar.YEAR);
        if (firstWage.get(Calendar.MONTH) > birthDate.get(Calendar.MONTH)) {
            age--;
        }
        query = "Select ID_periodicite from Entreprise where ID_entreprise = " + String.valueOf(s.getIdEntreprise());
        rs = stmt.executeQuery(query);
        int idPer = 1;
        if (rs.next()) {
            idPer = rs.getInt("ID_periodicite");
        }
        switch (idPer) {
            case 1:
                query = "Select `paiement mensuel` ";
                break;

            case 3:
                query = "Select `paiement trimestriel` ";
                break;

            case 6:
                query = "Select `paiement semestriel` ";
                break;

            case 12:
                query = "Select `paiement annuel` ";
                break;

            default:
                query = "Select `paiement annuel` ";
                break;
        }
        query += "from Tarif where x = " + String.valueOf(age);
        rs = stmt.executeQuery(query);
        if (rs.next()) {
            estimation = (float) (rs.getDouble(1) * taux);
        }
        rs.close();
        stmt.close();
        ConnectMySQL.closeConnection();
        return estimation;
    }
}
