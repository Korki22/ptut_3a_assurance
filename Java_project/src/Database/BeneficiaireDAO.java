/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import Model.Beneficiaire;
import Model.Salarie;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
/**
 *
 * @author Epulapp
 */
public class BeneficiaireDAO extends DAO<Beneficiaire> {

    static private DAO instance;

    private BeneficiaireDAO() {
    }

    public static BeneficiaireDAO getBeneficiaireDAO() {
        if (instance == null) {
            instance = new BeneficiaireDAO();
        }
        return (BeneficiaireDAO) instance;
    }

    @Override
    public Beneficiaire find(int id) throws SQLException {
        String query = "Select * from Beneficiaire where ID_beneficiaire = " + String.valueOf(id);
        Statement stmt;
        stmt = ConnectMySQL.getConnection().createStatement();
        ResultSet rs = stmt.executeQuery(query);
        Beneficiaire a = null;
        while (rs.next()) {
            a = new Beneficiaire(rs.getString("Lien_parente_beneficiaire"), rs.getFloat("Pourcentage_beneficiaire"), rs.getInt("ID_salarie"), rs.getInt("ID_beneficiaire"), rs.getString("Nom_beneficiaire"), rs.getString("Prenom_beneficiaire"), rs.getDate("Date_naissance_beneficiaire"));
        }
        rs.close();
        stmt.close();
        ConnectMySQL.closeConnection();
        return a;
    }

    @Override
    public void create(Beneficiaire obj) throws SQLException {
        String query = "INSERT INTO Beneficiaire ('ID_beneficiaire', 'Nom_beneficiaire', 'Prenom_beneficiaire', 'Date_naissance_beneficiaire', 'Lien_parente_beneficiaire', 'Pourcentage_beneficiaire', 'ID_salarie')"
                + "VALUES (NULL,?,?,?,?,?,?)";

        PreparedStatement stmt = ConnectMySQL.getConnection().prepareStatement(query);
        stmt.setString(1, obj.getNom());
        stmt.setString(2, obj.getPrenom());
        stmt.setDate(3, obj.getBirthdate());
        stmt.setString(4, obj.getLienParente());
        stmt.setFloat(5, obj.getPourcentage());
        stmt.setInt(6, obj.getIdSalarie());
        stmt.executeUpdate();
        stmt.close();
        ConnectMySQL.closeConnection();
    }

    @Override
    public void update(Beneficiaire obj) throws SQLException {
        Statement stmt;
        ResultSet rs;
        String query = "Select * from Beneficiaire where ID_beneficiaire = " + String.valueOf(obj.getId());
        stmt = ConnectMySQL.getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
        rs = stmt.executeQuery(query);
        if (rs.first()) {
            rs.updateString("Nom_beneficiaire", obj.getNom());
            rs.updateString("Prenom_beneficiaire", obj.getPrenom());
            rs.updateDate("Date_naissance_beneficiaire", obj.getBirthdate());
            rs.updateInt("ID_salarie", obj.getIdSalarie());
            rs.updateString("Lien_parente_beneficiaire", obj.getLienParente());
            rs.updateFloat("Pourcentage_beneficiaire", obj.getPourcentage());
            rs.updateRow();
            rs.close();
            stmt.close();
            ConnectMySQL.closeConnection();
        }
    }

    @Override
    public void delete(Beneficiaire obj) throws SQLException {
        String query = "Delete * from Beneficiaire where ID_beneficiaire = " + String.valueOf(obj.getId());
        Statement stmt;
        stmt = ConnectMySQL.getConnection().createStatement();
        stmt.executeUpdate(query);
        stmt.close();
        ConnectMySQL.closeConnection();
    }
    
    public ArrayList<Beneficiaire> getListBeneficiaire(Salarie s) throws SQLException
    {
         return getListBeneficiaire(s.getId());
    }
    
     public ArrayList<Beneficiaire> getListBeneficiaire(int id) throws SQLException
     {
         ArrayList<Beneficiaire> benef = new ArrayList<Beneficiaire>();
         String query = "Select * from Beneficiaire where ID_salarie = " + String.valueOf(id);
         Statement stmt = ConnectMySQL.getConnection().createStatement();
         ResultSet rs = stmt.executeQuery(query);
         while(rs.next())
         {
             benef.add(new Beneficiaire(rs.getString("Lien_parente_beneficiaire"), rs.getFloat("Pourcentage_beneficiaire"), rs.getInt("ID_salarie"),
                     rs.getInt("ID_beneficiaire"), rs.getString("Nom_beneficiaire"), rs.getString("Prenom_beneficiaire"), rs.getDate("Date_naissance_beneficiaire")));
         }
         rs.close();
         stmt.close();
         ConnectMySQL.closeConnection();
         return benef;
     }
}
