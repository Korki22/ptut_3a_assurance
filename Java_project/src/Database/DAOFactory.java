/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Database;

import Model.Maps;
import java.sql.SQLException;

/**
 *
 * @author Epulapp
 */

public class DAOFactory {

    public static AgenceDAO getAgenceDAO() {
        return AgenceDAO.getAgenceDAO();
    }

    public static AgentDAO getAgentDAO() {
        return AgentDAO.getAgentDAO();
    }

    public static SalarieDAO getSalarieDAO() throws SQLException {
        return SalarieDAO.getSalarieDAO();
    }

    public static BeneficiaireDAO getBeneficiaireDAO() {
        return BeneficiaireDAO.getBeneficiaireDAO();
    }
    
    public static EntrepriseDAO getEntrepriseDAO()throws SQLException {
        return EntrepriseDAO.getEntrepriseDAO();
    }
}
