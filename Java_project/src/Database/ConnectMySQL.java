/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 *
 * @author Win_V2
 */

public class ConnectMySQL {

    private static Logger logger = Logger.getLogger(ConnectMySQL.class);
    private static Connection instance;

    private ConnectMySQL() throws SQLException, FileNotFoundException, IOException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        Properties props = new Properties();
        FileInputStream file = new FileInputStream("src/connexion.properties");
        props.load(file);
        instance = DriverManager.getConnection("jdbc:mysql://" + props.getProperty("server") + "?zeroDateTimeBehavior=convertToNull&noAccessToProcedureBodies=true", props.getProperty("user"), props.getProperty("pwd"));
        file.close();
    }

    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    public static Connection getConnection() {
        if (instance == null) {
            try {
                new ConnectMySQL();
            } catch (SQLException | IOException | ClassNotFoundException ex) {
                System.out.println(ex.getMessage());
                logger.error("Failed to connect to database.", ex);
            }
        }
        return instance;
    }

    public static void closeConnection() throws SQLException {
        if(instance != null)
        {
            instance.close();
            instance = null;
        }
    }

}
