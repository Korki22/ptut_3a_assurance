/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import Model.Maps;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public abstract class DAO<T> {

    /**
     * Permet de récupérer un objet via son ID, return null si il n'existe aucun
     * oobjet pour l'id,, propage une exception si il y a eu une erreur
     *
     * @param id
     * @return
     * @throws java.sql.SQLException
     */
    public abstract T find(int id) throws SQLException;

    /**
     * Permet de créer une entrée dans la base de données par rapport à un objet
     *
     * @param obj
     */
    public abstract void create(T obj) throws SQLException;

    /**
     * Permet de mettre à jour les données d'une entrée dans la base
     *
     * @param obj
     */
    public abstract void update(T obj) throws SQLException;

    /**
     * Permet la suppression d'une entrée de la base
     *
     * @param obj
     */
    public abstract void delete(T obj) throws SQLException;
    
        
    public static void load() throws SQLException
    {
        Maps.instanciate();
        
        String query = "Select * from Situation_familiale";
        Statement stmt;
        stmt = ConnectMySQL.getConnection().createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
            Maps.addSituationFamiliale(rs.getInt("ID_situation_familiale"), rs.getString("Situation_familiale"));
        }

        query = "Select * from Option_de_prestation";
        //stmt = ConnectMySQL.getConnection().prepareStatement(query);
        rs = stmt.executeQuery(query);
        while (rs.next()) {
            Maps.addOptionPrestation(rs.getInt("ID_base_calcul"), rs.getString("Nom_base_calcul"));
        }

        query = "Select * from Statut";
        //stmt = ConnectMySQL.getConnection().prepareStatement(query);
        rs = stmt.executeQuery(query);
        while (rs.next()) {
            Maps.addSatut(rs.getInt("ID_statut"), rs.getString("Libelle_statut"), rs.getBoolean("Action_requise"));
        }

        query = "Select * from Mode_de_versement";
        //stmt = ConnectMySQL.getConnection().prepareStatement(query);
        rs = stmt.executeQuery(query);
        while (rs.next()) {
            Maps.addModeVersement(rs.getInt("ID_mode_versement"), rs.getString("Nom_mode_versement"));
        }
        
        query = "Select * from Periodicite";
        stmt = ConnectMySQL.getConnection().createStatement();
        rs = stmt.executeQuery(query);
        while (rs.next()) {
            Maps.addPeriodicite(rs.getInt("Nb_mois_periodicite"), rs.getString("Nom_periodicite"));
        }

        query = "Select * from Formule";
        //stmt = ConnectMySQL.getConnection().prepareStatement(query);
        rs = stmt.executeQuery(query);
        while (rs.next()) {
            Maps.addFormule(rs.getInt("ID_formule"), rs.getString("Nom_formule"));
        }
        
        query = "Select * from Agence";
        //stmt = ConnectMySQL.getConnection().prepareStatement(query);
        rs = stmt.executeQuery(query);
        while (rs.next()) {
            Maps.addAgence(rs.getInt("ID_agence"), rs.getString("Nom_agence"));
        }

        rs.close();
        stmt.close();
    }
}
