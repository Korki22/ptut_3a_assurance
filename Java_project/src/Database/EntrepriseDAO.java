/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import Model.Adresse;
import Model.Entreprise;
import Model.ListModel;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

/**
 *
 * @author Erwan
 */
public class EntrepriseDAO extends DAO<Entreprise> {

    static private DAO instance;

    static public EntrepriseDAO getEntrepriseDAO() throws SQLException {
        if (instance == null) {
            instance = new EntrepriseDAO();
        }
        return (EntrepriseDAO) instance;
    }

    @Override
    public Entreprise find(int id) throws SQLException {
        String query = "Select * from Entreprise_view where ID_entreprise = " + String.valueOf(id);
        Statement stmt;
        stmt = ConnectMySQL.getConnection().createStatement();
        ResultSet rs = stmt.executeQuery(query);
        Entreprise e = null;
        while (rs.next()) {
            Adresse ad = new Adresse(rs.getString("Nom_rue_entreprise"), rs.getString("Ville_entreprise"), rs.getString("CP_entreprise"));
            e = new Entreprise(rs.getInt("ID_entreprise"), rs.getString("Raison_sociale"), ad, rs.getString("Email_entreprise"), rs.getInt("Num_tel_entreprise"),
                    rs.getString("Pass_entreprise"), rs.getString("Num_siret"), rs.getInt("ID_periodicite"), rs.getInt("ID_formule"), rs.getInt("ID_statut"),
                    rs.getDate("Date_effet"), rs.getDate("Date_adhesion"), rs.getInt("ID_agence"), rs.getInt("Nb_salaries"));
        }
        rs.close();
        stmt.close();
        ConnectMySQL.closeConnection();
        return e;
    }

    @Override
    public void create(Entreprise obj) throws SQLException {
        String query = "INSERT INTO Entreprise('ID_entreprise', 'Raison_sociale', 'Ville_entreprise',"
                + "'CP_entreprise', 'Nom_rue_entreprise', 'Email_entreprise', 'Num_tel_entreprise',"
                + "'Pass_entreprise', 'Num_siret', 'ID_periodicite', 'ID_formule', 'ID_statut', 'Date_effet', 'Date_adhesion',"
                + "'ID_agence') VALUES  (NULL,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        PreparedStatement stmt = ConnectMySQL.getConnection().prepareStatement(query);
        stmt.setInt(1, obj.getId());
        stmt.setString(2, obj.getRaisonSociale());
        stmt.setString(3, obj.getAdresse().getVille());
        stmt.setString(4, obj.getAdresse().getCp());
        stmt.setString(5, obj.getAdresse().getVoie());
        stmt.setString(6, obj.getEmail());
        stmt.setInt(7, obj.getNumTel());
        stmt.setString(8, obj.getPass());
        stmt.setString(9, obj.getNumSiret());
        stmt.setInt(10, obj.getIdPer());
        stmt.setInt(11, obj.getIdForm());
        stmt.setInt(12, obj.getIdStatut());
        stmt.setDate(13, obj.getDateEffet());
        stmt.setDate(14, obj.getDateAdh());
        stmt.setInt(15, obj.getIdAgence());
        stmt.executeUpdate();
        stmt.close();
        ConnectMySQL.closeConnection();
    }

    @Override
    public void update(Entreprise obj) throws SQLException {
        Statement stmt;
        ResultSet rs;
        String query = "Select * from Entreprise where ID_entreprise = " + String.valueOf(obj.getId());
        stmt = ConnectMySQL.getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
        rs = stmt.executeQuery(query);
        if (rs.first()) {
            rs.updateString("Raison_sociale", obj.getRaisonSociale());
            rs.updateString("Ville_entreprise", obj.getAdresse().getVille());
            rs.updateString("CP_entreprise", obj.getAdresse().getCp());
            rs.updateString("Nom_rue_entreprise", obj.getAdresse().getVoie());
            rs.updateString("Email_entreprise", obj.getEmail());
            rs.updateInt("Num_tel_entreprise", obj.getNumTel());
            rs.updateString("Pass_entreprise", obj.getPass());
            rs.updateString("Num_siret", obj.getNumSiret());
            rs.updateInt("ID_periodicite", obj.getIdPer());
            rs.updateInt("ID_formule", obj.getIdForm());
            rs.updateInt("ID_statut", obj.getIdStatut());
            rs.updateDate("Date_effet", obj.getDateEffet());
            rs.updateDate("Date_adhesion", obj.getDateAdh());
            rs.updateInt("ID_agence", obj.getIdAgence());
            rs.updateRow();
            rs.close();
            stmt.close();
            ConnectMySQL.closeConnection();
        }
    }

    @Override
    public void delete(Entreprise obj) throws SQLException {
        String query = "Delete * from Entreprise where ID_entreprise = " + String.valueOf(obj.getId());
        Statement stmt;
        stmt = ConnectMySQL.getConnection().createStatement();
        stmt.executeUpdate(query);
        stmt.close();
        ConnectMySQL.closeConnection();
    }

    /**
     * @param list une liste à remplir
     * @param name la raison sociale de l'entreprise à rechercher
     * @return le nombre d'entreprise chargée dans la liste
     * @throws SQLException
     */
    public int SearchEntrepriseByName(ListModel<Entreprise> list, String name, int rowNumber, int count, boolean action_needed) throws SQLException {
        String query = "Select * from Entreprise_view";
        String args = "";
        if (!name.equals("")) {
            args += " where Raison_sociale LIKE '%" + name + "%'";
        }

        //Params...
        if (action_needed) {
            if (!args.equals("")) {
                args += " AND ";
            } else {
                args = " WHERE ";
            }
            args += "`ID_statut` IN (Select ID_statut from Statut where Action_requise = 1)";
        }
        query += args;

        if (rowNumber >= 0 && count > 0) {
            query += " LIMIT " + String.valueOf(rowNumber) + ", " + String.valueOf(count);
        }

        Statement stmt;
        stmt = ConnectMySQL.getConnection().createStatement();
        ResultSet rs = stmt.executeQuery(query);
        System.out.println(query);
        list.clear();
        while (rs.next()) {
            Adresse ad = new Adresse(rs.getString("Nom_rue_entreprise"), rs.getString("Ville_entreprise"), rs.getString("CP_entreprise"));
            list.addItem(new Entreprise(rs.getInt("ID_entreprise"), rs.getString("Raison_sociale"), ad, rs.getString("Email_entreprise"), rs.getInt("Num_tel_entreprise"),
                    rs.getString("Pass_entreprise"), rs.getString("Num_siret"), rs.getInt("ID_periodicite"), rs.getInt("ID_formule"), rs.getInt("ID_statut"),
                    rs.getDate("Date_effet"), rs.getDate("Date_adhesion"), rs.getInt("ID_agence"), rs.getInt("Nb_salaries")));
        }

        String queryForCount = "Select count(*) from Entreprise_view";
        queryForCount += args;

        rs = stmt.executeQuery(queryForCount);
        int nbRes = -1;
        if (rs.first()) {
            nbRes = rs.getInt(1);
        }
        rs.close();
        stmt.close();
        ConnectMySQL.closeConnection();
        return nbRes;
    }

    public float getPrimePerYear(Entreprise ent, int year) throws SQLException {
        return getPrimePerYear(ent.getId(), year);
    }

    public float getPrimePerYear(int idEnt, int year) throws SQLException {
        String query = "{? = call getPrimeEntreprise(?, ?)}";
        CallableStatement cs = ConnectMySQL.getConnection().prepareCall(query);
        cs.registerOutParameter(1, Types.FLOAT);
        cs.setInt(2, idEnt);
        cs.setInt(3, year);
        cs.execute();
        float result = cs.getFloat(1);
        cs.close();
        ConnectMySQL.closeConnection();
        return result;
    }

    public float getMontantAvenant(int idEnt, int idPer) throws SQLException {
        String query = "Select ID_salarie from Salarie where ID_entreprise = " + String.valueOf(idEnt);
        Statement stmt;
        stmt = ConnectMySQL.getConnection().createStatement();
        ResultSet rs = stmt.executeQuery(query);
        float total = 0;
        while (rs.next()) {
            int idSal = rs.getInt("ID_salarie");
            String querySal = "SELECT Cotisation FROM Salarie WHERE `ID_salarie` = " + String.valueOf(idSal);
            Statement stmtSal;
            stmtSal = ConnectMySQL.getConnection().createStatement();
            ResultSet rsSal = stmtSal.executeQuery(querySal);
            if (rsSal.next()) {
                total += rsSal.getDouble("Cotisation") / (12/idPer);
            }
            rsSal.close();
            stmtSal.close();
        }
        rs.close();
        stmt.close();
        ConnectMySQL.closeConnection();
        return total;
    }
}
