/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import Model.Agence;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Epulapp
 */
public class AgenceDAO extends DAO<Agence> {

    static private DAO instance;

    private AgenceDAO() {
    }

    public static AgenceDAO getAgenceDAO() {
        if (instance == null) {
            instance = new AgenceDAO();
        }
        return (AgenceDAO) instance;
    }

    @Override
    public Agence find(int id) throws SQLException {
        String query = "Select * from Agence where ID_agence = " + String.valueOf(id);
        Statement stmt;
        stmt = ConnectMySQL.getConnection().createStatement();
        ResultSet rs = stmt.executeQuery(query);
        Agence a = null;
        while (rs.next()) {
            a = new Agence(rs.getInt("ID_agence"), rs.getString("Nom_agence"));
        }
        rs.close();
        stmt.close();
        ConnectMySQL.closeConnection();
        return a;
    }

    @Override
    public void create(Agence obj) throws SQLException {
        String query = "INSERT INTO Agence ('ID_agence', 'Nom_agence')"
                + "VALUES (NULL,?)";

        PreparedStatement stmt = ConnectMySQL.getConnection().prepareStatement(query);
        stmt.setString(1, obj.getNom());
        stmt.executeUpdate();
        stmt.close();
        ConnectMySQL.closeConnection();
    }

    @Override
    public void update(Agence obj) throws SQLException {
        Statement stmt;
        ResultSet rs;
        String query = "Select * from Agence where ID_agence = " + String.valueOf(obj.getId());
        stmt = ConnectMySQL.getConnection().createStatement();
        rs = stmt.executeQuery(query);
        if (rs.first()) {
            rs.updateString("Nom_agence", obj.getNom());
            rs.updateRow();
            rs.close();
            stmt.close();
            ConnectMySQL.closeConnection();
        }
    }

    @Override
    public void delete(Agence obj) throws SQLException {
        String query = "Delete * from Agence where ID_agencenm = " + String.valueOf(obj.getId());
        Statement stmt;
        stmt = ConnectMySQL.getConnection().createStatement();
        stmt.executeUpdate(query);
        stmt.close();
        ConnectMySQL.closeConnection();
    }

}
