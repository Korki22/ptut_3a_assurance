/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

import Database.AgenceDAO;
import Database.SalarieDAO;
import Model.Entreprise;
import Model.GenericModel;
import Model.Maps;
import Model.Salarie;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author Win_V2
 */
public class ModelFormater {
    
    public static Object[][] getData(GenericModel model) throws SQLException {
        if(model instanceof Salarie) {
         Salarie s = ((Salarie)model);
         
         JComboBox situationFamilialeCombo = new JComboBox<String>(Maps.situationFamilialeValues());
         situationFamilialeCombo.setSelectedItem(Maps.getSituationFamialie(s.getIdSitFam()));
         
         Object[][] data = {
            {"Nom :", s.getNom(), true},
            {"Prenom :", s.getPrenom(), true},
            {"Entreprise :", s.getIdEntreprise(),true},
            {"Rue :", s.getAdresse().getVoie(),true},
            {"Code postal :", s.getAdresse().getCp(),true}, 
            {"Ville :", s.getAdresse().getVille(),true},
            {"Email :", s.getEmail(),true},
            {"Numéro de téléphone :",s.getNumTel(),true},
            {"Situation familiale :", situationFamilialeCombo,true},
            {"Agence de référence :", Maps.getAgence(s.getIdAgence()),false},
            {"Formule du contrat :", Maps.getFormule(s.getIdFormule()),false},
            {"Statut :", Maps.getStatut(s.getIdStatut()),false},
            {"Montant des cotisations :", s.getCotisation(), true},
            {"Estimation des cotisation percues",SalarieDAO.getSalarieDAO().estimationViagere(s),false}
                 
        };
        return data;
    } else if (model instanceof Entreprise) {
        Entreprise e = ((Entreprise)model);
         
         Object[][] data = {
            {"Raison sociale :", e.getRaisonSociale(), true},
            {"Titre :", e.getTitle(), true},
            {"Rue :", e.getAdresse().getVoie(),true},
            {"Code postal :", e.getAdresse().getCp(),true}, 
            {"Ville :", e.getAdresse().getVille(),true},
            {"Email :", e.getEmail(),true},
            {"Numéro de téléphone :",e.getNumTel(),true},
           
            {"Agence de référence :", Maps.getAgence(e.getIdAgence()),false},
           
            {"Statut :", Maps.getStatut(e.getIdStatut()),false}
        };
        return data;
        
        };
        return null;
        } 
}
