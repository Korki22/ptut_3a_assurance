/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

import Database.DAOFactory;
import Model.Entreprise;
import Model.Salarie;
import com.ibm.icu.text.NumberFormat;
import com.ibm.icu.text.RuleBasedNumberFormat;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.sql.Date;
import java.util.Locale;
import org.apache.log4j.Logger;

/**
 *
 * @author Moi
 */
public class PDFGenerator {

    private static Logger logger = Logger.getLogger(PDFGenerator.class);
    private static Font titre1 = new Font(Font.FontFamily.TIMES_ROMAN, 15,
            Font.BOLD);
    private static Font titre2 = new Font(Font.FontFamily.TIMES_ROMAN, 13,
            Font.NORMAL);
    private static Font texte = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.NORMAL);

    public static void exportAttestationFiscale(Salarie salarie, String filePath, Date date) {

        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(filePath));
            document.open();
            addTitlePageAttestation(document, salarie, date);
            addContentAttestation(document, salarie, date.getYear());
            document.close();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    private static void addTitlePageAttestation(Document document, Salarie salarie, Date date)
            throws DocumentException {
        Paragraph preface = new Paragraph();
        // We add one empty line
        addEmptyLine(preface, 1);
        // En-tête
        // Date
        //preface.add(date.setAlignment(Element.ALIGN_RIGHT));
        // Titres 
        preface.add(new Paragraph("DIRECTION TECHNIQUE OU ACTUARIAT", titre2));
        preface.add(new Paragraph("CONTRACTANTE : ASSURAMORT COMPANY CORP", titre2));
        addEmptyLine(preface, 2);
        // Titre gras centré
        Paragraph temp = new Paragraph("PRODUIT RETRAITE SUPPLEMENTAIRE", titre1);
        temp.setAlignment(Element.ALIGN_CENTER);
        preface.add(temp);

        temp = new Paragraph("AVEC CONTRE ASSURANCE DES PRIMES", titre1);
        temp.setAlignment(Element.ALIGN_CENTER);
        temp.setFont(titre1);
        preface.add(temp);
        addEmptyLine(preface, 2);
        // Police d'assurance
        
        temp=new Paragraph("POLICE D’ASSURANCE : N°" + date.getYear() +"."+ salarie.getIdAgence()+"."+"001"+".", titre2);
        preface.add(temp);
  
        preface.add(new Paragraph("DATE  D’EFFET :" + date.toString(), titre2));
        // Durée d'assurance
        preface.add(new Paragraph("DUREE D’ASSURANCE : ", titre2));
        // N° d'adhésion
        preface.add(new Paragraph("N° D’ADHESION :"+ salarie.getId(), titre2));
        addEmptyLine(preface, 1);
        // Titre gras centré principal
        temp = new Paragraph("ATTESTATION DE VERSEMENT", titre1);
        temp.setAlignment(Element.ALIGN_CENTER);
        preface.add(temp);
        addEmptyLine(preface, 4);

        document.add(preface);

    }

    private static void addContentAttestation(Document document, Salarie salarie, int _year) throws DocumentException, SQLException {
        Paragraph para = new Paragraph();
NumberFormat formatter = new RuleBasedNumberFormat(Locale.FRANCE, RuleBasedNumberFormat.SPELLOUT);
        Paragraph temp = new Paragraph("Nous, ASSURAMORT COMPANY CORP attestons par la présente que l’adhérent(e) "
                .concat(salarie.getNom() + " ")
                .concat(salarie.getPrenom())
                .concat(" au produit Retraite Supplémentaire avec contre assurance des primes s’est acquitté(e), au titre de l’exercice ")
                .concat(_year +"")
                .concat(" , d’un montant total de cotisations de ")
                .concat(String.valueOf(salarie.getInfo().getCotisationPerYear(2014))).concat(" euros, soit en toutes lettres : ")
                .concat(formatter.format(salarie.getInfo().getCotisationPerYear(2014) )+ " euros"), texte);

        temp.setAlignment(Element.ALIGN_JUSTIFIED);
        para.add(temp);

        addEmptyLine(para, 2);
        temp = new Paragraph("Cette attestation lui est délivrée pour servir et valoir ce que de droit.", texte);
        temp.setAlignment(Element.ALIGN_RIGHT);
        para.add(temp);

        // now add all this to the document
        document.add(para);

    }

    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
// //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    
    
    
    public static void ExportAvenant(Entreprise entreprise, String filePath, Date date) {
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(filePath));
            document.open();
            addTitlePageAvenant(document);
            addContentAvenant(document, entreprise, date);
            document.close();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    private static void addTitlePageAvenant(Document document)
            throws DocumentException {
        Paragraph preface = new Paragraph();
        // We add one empty line
        addEmptyLine(preface, 1);

	// En-tête
        // logo
        // titres
        preface.add(new Paragraph("SERVICE ASSURANCE DE PERSONNES", titre1));
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("AVENANT DE RESSORTIE DE PRIMES", titre1));
        addEmptyLine(preface, 1);
        document.add(preface);
    }

    private static void addContentAvenant(Document document, Entreprise entreprise, Date date) throws DocumentException, SQLException {
        Paragraph para = new Paragraph();
        // We add one empty line
        addEmptyLine(para, 1);

        // Création et ajout du tableau
        createTable(document, date, entreprise);

        addEmptyLine(para, 1);
        
        //Article 1
        Paragraph temp = new Paragraph("ART 1 - "
                .concat("Le présent avenant a pour objet de ressortir la prime d’assurance Retraite complémentaire, concernant le personnel : ")
                .concat(entreprise.getRaisonSociale())
                .concat(" , pour le mois de ")
                .concat(new SimpleDateFormat("MMMM").format(date))
                .concat(date.getYear()+""), texte);
                
        para.add(temp);

        //prime
        temp = new Paragraph("Prime nette : "
                .concat(Float.toString(DAOFactory.getEntrepriseDAO().getMontantAvenant(entreprise.getId(), entreprise.getIdPer())))
                .concat("euros"), texte);
        para.add(temp);

        // Article 2
        temp = new Paragraph("ART 2 – ".concat("Il n’est pas autrement dérogé aux autres clauses et conditions du contrat sus visé."), texte);
        para.add(temp);

        // Article 3
        NumberFormat formatter = new RuleBasedNumberFormat(Locale.FRANCE, RuleBasedNumberFormat.SPELLOUT);
        temp = new Paragraph("ART 3 – ".concat("Au titre du présent avenant, il sera perçu la somme de :").concat(formatter.format(DAOFactory.getEntrepriseDAO().getPrimePerYear(entreprise, date.getYear()))+ "euros").concat("("+DAOFactory.getEntrepriseDAO().getPrimePerYear(entreprise, date.getYear())+")" ), texte);
        para.add(temp);

        addEmptyLine(para, 3);

        // Bas de page
        temp = new Paragraph("Fait  à Lyon , le : ……………………………..", texte);
        temp.setAlignment(Element.ALIGN_RIGHT);
        para.add(temp);

        addEmptyLine(para, 1);

        temp = new Paragraph("LE SOUSCRIPTEUR                                                          L’ASSUREUR", texte);
        para.add(temp);

        document.add(para);

    }

    private static void createTable(Document document, Date date, Entreprise entreprise)
            throws BadElementException, SQLException, DocumentException {
        PdfPTable table = new PdfPTable(4);


        Phrase contenu1 = new Phrase("C.Emission : ", titre2);
        Phrase contenu2 = new Phrase("Lyon I", texte);
        PdfPCell c1 = new PdfPCell();
        c1.addElement(contenu1);
        c1.addElement(contenu2);
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        //Bureau
        contenu1 = new Phrase("C.Bureau : ", titre2);
        contenu2 = new Phrase("12", texte);
        c1 = new PdfPCell();
        c1.addElement(contenu1);
        c1.addElement(contenu2);
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        //N° Police
        contenu1 = new Phrase("N° Police : ", titre2);
        contenu2 = new Phrase(date.getYear()+"".concat(".")
                .concat(entreprise.getIdAgence()+".")
                .concat(entreprise.getIdForm()+".")
                .concat(date.getMonth()+""), texte);
        c1 = new PdfPCell();
        c1.addElement(contenu1);
        c1.addElement(contenu2);
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        // Avenant
        contenu1 = new Phrase("Avenant : ", titre2);
        contenu2 = new Phrase(date.getYear()+"".concat(".")
                .concat(entreprise.getIdForm()+".")
                .concat(date.getMonth()+""), texte);

        c1 = new PdfPCell();
        c1.addElement(contenu1);
        c1.addElement(contenu2);
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);
        // première ligne ajoutée
        table.setHeaderRows(1);

	// Deuxième ligne
        //colonne 1
        contenu1 = new Phrase("Assuré :", titre2);
        contenu2 = new Phrase(entreprise.getRaisonSociale(), texte);
        c1 = new PdfPCell();
        c1.addElement(contenu1);
        c1.addElement(contenu2);
        table.addCell(c1);

        //colonne 2
        table.addCell(" ");

        //colonne 3
        table.addCell(" ");

        //colonne 4
        table.addCell(" ");

	// Troisième ligne	
        //colonne 1
        contenu1 = new Phrase("Adresse :", titre2);
        contenu2 = new Phrase(entreprise.getAdresse().toString(), texte);
        c1 = new PdfPCell();
        c1.addElement(contenu1);
        c1.addElement(contenu2);
        table.addCell(c1);

        //colonne 2
        table.addCell(" ");

        //colonne 3
        table.addCell(" ");

        //colonne 4
        table.addCell(" ");

	// Quatrième ligne
        //colonne 1
        contenu1 = new Phrase("Effet :", titre2);
        contenu2 = new Phrase("01/"+date.getMonth()+"/"+date.getYear(), texte);
        
		 c1 = new PdfPCell();
        c1.addElement(contenu1);
        c1.addElement(contenu2);
        table.addCell(c1);

        //colonne 2
        table.addCell(" ");

        //colonne 3
        contenu1 = new Phrase("Echéance :", titre2);
        contenu2 = new Phrase("Viagère", texte);
        
		c1 = new PdfPCell();
        c1.addElement(contenu1);
        c1.addElement(contenu2);
        table.addCell(c1);

        //colonne 4
        contenu1 = new Phrase("Fractionement :", titre2);
        contenu2 = new Phrase(entreprise.getIdPer()+"", texte);
        
	c1 = new PdfPCell();
        c1.addElement(contenu1);
        c1.addElement(contenu2);
        table.addCell(c1);
        
        //récupération de la prime à payer pour l'entreprise
        String prime = String.valueOf(DAOFactory.getEntrepriseDAO().getMontantAvenant(entreprise.getId(), entreprise.getIdPer()));

		// Cinquième ligne
        //colonne 1
        contenu1 = new Phrase("Prime nette : " + prime, titre2);
        contenu2 = new Phrase(DAOFactory.getEntrepriseDAO().getPrimePerYear(entreprise, date.getYear())+"", texte);
        c1 = new PdfPCell();
        c1.addElement(contenu1);
        c1.addElement(contenu2);
        table.addCell(c1);

        //colonne 2
        table.addCell(" ");

        //colonne 3
        contenu1 = new Phrase("C/Police :", titre2);
        contenu2 = new Phrase("40 ,00 euros ", texte);
        
		c1 = new PdfPCell();
        c1.addElement(contenu1);
        c1.addElement(contenu2);
        table.addCell(c1);

        //colonne 4
        contenu1 = new Phrase("Taxe :          /", texte);
        c1 = new PdfPCell();
        c1.addElement(contenu1);
        table.addCell(c1);

		// Sixième ligne
        //colonne 1
        contenu1 = new Phrase("Prime globale : " + prime, titre2);
        contenu2 = new Phrase(DAOFactory.getEntrepriseDAO().getPrimePerYear(entreprise, date.getYear())+"", texte);
        c1 = new PdfPCell();
        c1.addElement(contenu1);
        c1.addElement(contenu2);
        table.addCell(c1);

        //colonne 2
        table.addCell(" ");

        //colonne 3
        contenu1 = new Phrase("D/Timbre :", titre2);
        contenu2 = new Phrase("40 ,00 euros ", texte);
        
		c1 = new PdfPCell();
        c1.addElement(contenu1);
        c1.addElement(contenu2);
        table.addCell(c1);

        //colonne 4
        table.addCell(" ");

		// Septième ligne
        //colonne 1
        contenu1 = new Phrase("Prime Totale : " + prime, titre2);
        contenu2 = new Phrase(DAOFactory.getEntrepriseDAO().getPrimePerYear(entreprise, date.getYear())+"", texte);
        c1 = new PdfPCell();
        c1.addElement(contenu1);
        c1.addElement(contenu2);
        table.addCell(c1);

        //colonne 2
        table.addCell(" ");

        //colonne 3
        table.addCell(" ");

        //colonne 4
        table.addCell(" ");

        document.add(table);

    }

}

