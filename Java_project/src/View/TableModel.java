/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package View;

import Model.ListModel;
import java.util.Observable;
import java.util.Observer;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Epulapp
 */
public class TableModel extends AbstractTableModel implements Observer{
    
    ListModel list;

    public TableModel(ListModel list) {
        this.list = list;
    }
    
    @Override
    public int getRowCount() {
        if (list == null ) return 0;
        return list.getSize();
    }

    @Override
    public int getColumnCount() {
        if (list == null ) return 0;
        return list.getColumnCount();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return list.getValueAt(rowIndex,columnIndex);
    }
    
    @Override
    public String getColumnName(int column) {
   
        String header[] = list.getHeader();
        String name = (column < header.length) ? header[column] : "Undefined";
        return name;
    }

    @Override
    public void update(Observable o, Object arg) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public Object getItem(int row) {
        return list.getItem(row);
    }
    
}
