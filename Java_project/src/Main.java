/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import Database.DAO;
import Database.DAOFactory;
import Database.SalarieDAO;
import View.LoginWindow;
import java.awt.Color;
import java.sql.SQLException;
import java.util.logging.Level;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import org.apache.log4j.Logger;

/**
 * https://lite5.framapad.org/p/lesravioliscestdelicieux
 * @author Epulapp
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    
    private static Logger logger = Logger.getLogger(Main.class);
   
    public static void main(String[] args){
        /* set the nimbus look and feel*/
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    UIManager.getLookAndFeelDefaults().put("Panel.background", new Color(240,240,240));
                    UIManager.getLookAndFeelDefaults().put("nimbusBase", new Color(255,0,0));
                     break;
                }
            }
        } catch (ClassNotFoundException ex) {
            logger.error(ex.getMessage(), ex);
        } catch (InstantiationException ex) {
            logger.error(ex.getMessage(), ex);
        } catch (IllegalAccessException ex) {
            logger.error(ex.getMessage(), ex);
        } catch (UnsupportedLookAndFeelException ex) {
            logger.error(ex.getMessage(), ex);
        }
        LoginWindow window  = new LoginWindow();
        window.setVisible(true);

    }
}
