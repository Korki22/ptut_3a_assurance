﻿*********************************************************PARTIE WEB***********************************************


**Serveur chez Florentin Audebert. De ce fait, il est possible que les ports su serveur ne soient pas ouvert.
**Ainsi, pour les différents tests, il est plus judicieux de passer en local.
**Utilisation de WAMP Server.
**Installation : http://www.wampserver.com/

**Une fois le serveur local en route, il suffit d'utiliser l'URL ci_dessous pour se rendre à la page d'accueil. (Nécéssité de mettre tout les fichiers de la partie PHP
**sous le dossier www placé dans C://
**URL : http://localhost/ptut_3a_assurance/php/ptut_3a_assurance/index.php?content=accueil.
-----------------------------------------------
ENtreprise
-----------------------------------------------
**Puis il est possible d'inscrire en allant dans l'onglet Inscription.
**Bien se rappeler des identifiant si la connexion est souhaité.
**(Si l'inscription fonctionne est qu'une redirection semble louche, retourner à l'accueil puis cliquer sur Connexion avec les identifiants choisis juste avant).
**Le panneau de configuration permet d'accéder aux diverses fonctionnalités une fois l'entreprise connectée.
**Pour rajouter des salariés : Il faut importer un fichier .csv 
**(Bien l'avoir rempli avant avec seulement 3 champs et un séparateur virgule ',' --  list1.csv est mis en exemple.)
**Les autres fonctionnalités de calculs et d'inmpressions d'avenant directement par l'entreprise ne sont pas encore implémentés, mais peuvent faire l'objet d'une amélioration
**future.
-----------------------------------------------
Salarié
-----------------------------------------------
- Le salarié se connecte avec les identifiants qu'il a reçu par mail (par défault le mot de passe est "toto")
- Le salarié accède à ses informations personnelles avec l'onglet "Informations personnelles"
- Les champs de la page sont modifiables et les modifications validées par le bouton "Modifier mes données"
- Le salarié peut supprimer un beneficiaire à l'aide de la croix au bout de la ligne
- Le salarié peut ajouter un beneficiaire à l'aide des champs de la ligne et du bouton + au bout de la ligne
--- Consulter les cotisations déjà versées ----
- L'onglet cotisations affiche une page avec un tableau des cotisations déjà versées par le salarié
-----------------------------------------------
